module Jekyll
  class ShellCommand < Liquid::Tag

    def initialize(tag_name, text, tokens)
      super
      @text = text
    end

    def render_variable(context)
      Liquid::Template.parse(@text).render(context)
    end

    def render(context)
      # text = render_variable(@text)
      `#{@text}`.strip
    end
  end
end

Liquid::Template.register_tag('shellcmd', Jekyll::ShellCommand)
