#+LATEX_HEADER: \setlength{\parindent}{0pt} \setlength{\parskip}{6pt plus 2pt minus 1pt}

* Fala lógica

Esta obra tem como objectivo futuro reestruturar o modelo actual da
fala.

A fala Portuguesa está assente num modelo maioritariamente dualista.
Contente, descontente, completo, incompleto, medricas, valente.
Contrários. 2 elementos.

Pouco prevalentes são os casos de trindade, 3 elementos, como, ácido,
neutro, básico (em acidez); agudo, recto, obtuso (em amplitude angular);
etc..

Em resposta num diálogo, o sim traz o positivo, o não o negativo; e não
existe actualmente uma só palavra a significar neutro, a significar "não
sim e não não". Tal palavra poderá ser criada.

** A /Negação do Vazio/

Muitas pessoas dizem: "não quero nada", "não gosto de ninguém".

Isto é /negação do vazio/. Pois tem /não/ e um derivado de /nada/ (nada,
ninguém, nenhum).

Vou envolver-me na análise de "não quero nada" por exemplo. Existe o
/nada/, o /algo/, o /tudo/. Dizer que "não quer nada" significa que quer
algo ou quer tudo. E isso não é realmente o intuito que quer-se emitir.
Quer-se emitir "quero nada" ou "não quero". Da mesma maneira "não gosto
de alguém" pode ser uma das maneiras exactas de falar (ao invés de "não
gosto de ninguém").

** O /oposto/ e o /não-algo/

Utilizo uma casa quadrada como exemplo. Tem uma fachada principal (1),
um lado esquerdo (2), uma fachada traseira (3), um lado direito (4).

O oposto da fachada principal é a fachada traseira.

O "não fachada principal" é o lado esquerdo e a fachada traseira e o
lado direito.

Logo oposto de fachada principal é diferente de "não fachada principal".
A "não fachada principal" é, neste exemplo, o conceito não-algo.

Num sistema dualista (binário), tal como a maioria das palavras
Portuguesas -- pois são acentes no /sim/ ou /não/ -- o oposto e o
não-algo confundem-se como idênticos.

Não o são; apenas o são em sistemas dualista (binários).

** A /negação na pergunta/

A pergunta "Não queres isso?" quando respondida com "sim" significa não
querer isso; respondida com "não" significa /não-algo/ (no sistema
falante dualista significa "querer isso").

Muitas vezes responde-se com "não" significando "não querer isso". Tal
resposta está incorrecta.

Uma maneira de clarificar a resposta é dizer, "não, quero sim", ou
então "sim, não quero".

Se for o questionador, faça a pergunta na afirmação (invés de usar o
/não/). Assim: "Queres isso?".

** A /Fala livre/

Evita dar comandos, instruções, ordens, a qualquer pessoa. Tenta
eliminar os verbos, /ter/, /dever/, etc. ; verbos que indiquem
/obrigação/. Deixa de lado o imperativo. Reforça o lado declarativo. Sem
usar pré-condicionamentos.

Tenta atingir a compreensão de o receptor da fala e o emissor da fala.
Quem fala pode estar disposto a receber, a aprender ... a
questionar(-se). A /Fala livre/ demonstra respeito pelo receptor da
fala.

Se for muito esforço inaceitável falar assim, não se esforçar talvez
seja o melhor caminho. Você leu; logo não sairá (de si). /Fala livre/,
se desejar.

*Palavras-chave:* dispensar (em vez de negar), algo, alguém, algum,
assim, exacto, neutro, isso
