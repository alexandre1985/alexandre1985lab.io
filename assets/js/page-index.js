// Copyright (C) 2024 Daniel Cerqueira

// This file is part of alexandre1985.gitlab.io.

// alexandre1985.gitlab.io is free software: you can redistribute it
// and/or modify it under the terms of the GNU Affero General Public
// License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.

// alexandre1985.gitlab.io is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public
// License along with alexandre1985.gitlab.io. If not, see
// <https://www.gnu.org/licenses/>.


// Navbar colors
let navbarPrimaryColor = 'navbar-color'
let navbarSecondColor = 'navbar-second-color'


function navbarColorToggle(arryEls, action) {
    if(action === 'primary') {

        arryEls.forEach(el => el.classList.remove(navbarSecondColor))
        arryEls.forEach(el => el.classList.add(navbarPrimaryColor))

    } else if(action === 'secondary') {

        arryEls.forEach(el => el.classList.remove(navbarPrimaryColor))
        arryEls.forEach(el => el.classList.add(navbarSecondColor))

    } else {

        arryEls.forEach(el => el.classList.toggle(navbarPrimaryColor))
        arryEls.forEach(el => el.classList.toggle(navbarSecondColor))

    }
}

document.addEventListener('DOMContentLoaded', function() {

    let navbarColorElements = $a('.navbar-color')

    let targetPointOfChange = $o('#hero').offsetHeight * 0.8;


    /* Change navbar color on page refresh */
    if(window.scrollY <= targetPointOfChange) {
        navbarColorToggle(navbarColorElements, 'primary')
    } else {
        navbarColorToggle(navbarColorElements, 'secondary')
    }

    /* Change navbar color on scroll */
    document.addEventListener('scroll', function () {
        if(window.scrollY <= targetPointOfChange) {
            navbarColorToggle(navbarColorElements, 'primary')
        } else {
            navbarColorToggle(navbarColorElements, 'secondary')
        }

    }, {passive: true});

    /* Remove transparent when hamburguer button or brand link click */
    $o('.navbar-toggler').addEventListener('click', function() {
        if(window.scrollY <= targetPointOfChange) {
            navbarColorToggle(navbarColorElements)
        }
    });
    $o('.navbar-brand').addEventListener('click', function() {
        if(window.scrollY <= 10) {
            navbarColorToggle(navbarColorElements, 'primary')
        }
    });


    /* mobile swip carousel */

    let carousel = $o('.carousel');
    let carouselInner = $o('.carousel-inner');
    let commentarySwipeSound = new Audio('/assets/audio/water-swipe.mp3');
    let xClick=0;

    function carouselSlide(direction) {
        let activeSlide = $o('.carousel .carousel-item.active')
        activeSlide.classList.remove('active')

        let targetSlide
        if(direction === 'next') {
            targetSlide = activeSlide.nextElementSibling ? activeSlide.nextElementSibling : $o('.carousel-item:first-child')
        } else if(direction === 'prev') {
            targetSlide = activeSlide.previousElementSibling ? activeSlide.previousElementSibling : $o('.carousel-item:last-child')
        }

        targetSlide.classList.add('active')
    }

    let carouselDirection = null


    carouselInner.addEventListener('touchstart', function(event){
        xClick = event.changedTouches[0].pageX;
    }, {passive: true});

    carouselInner.addEventListener('touchend', function() {
        if (carouselDirection) {
            carouselSlide(carouselDirection)
            commentarySwipeSound.play(); // play sound
            carouselDirection = null
        }
    }, {passive: true});

    carouselInner.addEventListener('touchmove', function(event){
        let xMove = event.changedTouches[0].pageX;
        let xInterval = Math.floor(xClick - xMove)

        if( xInterval > 77 ){
            carouselDirection = 'next'
        }
        else if( xInterval < -77 ){
            carouselDirection = 'prev'
        }
    }, {passive: true});


    /* carousel click move/slide and sound */
    var carouselClickSound = new Audio('/assets/audio/water-drop.mp3');

    let carouselControls = $a('.carousel-control')

    for (let i = 0; i < carouselControls.length; i++) {

        carouselControls[i].addEventListener('click', function(evt) {
            let button = evt.target;
            let buttonDirection = button.dataset.slide

            carouselSlide(buttonDirection)
            carouselClickSound.play() // play sound
        });
    }

    /* CV sound */
    var CVSound = new Audio('/assets/audio/bell-in.mp3');
    let soundButtons = $a('.sound-button')
    for (var i = 0; i < soundButtons.length; i++) {
        let soundButton = soundButtons[i]

        soundButton.addEventListener('mouseenter', function() {
            CVSound.play();
        });

        soundButton.addEventListener('click', function(evt) {
            evt.preventDefault()
            setTimeout(() => {
                let link = evt.target.href
                window.location.href = link
            }, 700)
        });

    }

});
