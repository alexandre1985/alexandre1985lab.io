// Copyright (C) 2024 Daniel Cerqueira

// This file is part of alexandre1985.gitlab.io.

// alexandre1985.gitlab.io is free software: you can redistribute it
// and/or modify it under the terms of the GNU Affero General Public
// License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.

// alexandre1985.gitlab.io is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public
// License along with alexandre1985.gitlab.io. If not, see
// <https://www.gnu.org/licenses/>.


sessionStorage.setItem('blog-last-page', 'true')
