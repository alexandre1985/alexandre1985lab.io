// Copyright (C) 2024 Daniel Cerqueira

// This file is part of alexandre1985.gitlab.io.

// alexandre1985.gitlab.io is free software: you can redistribute it
// and/or modify it under the terms of the GNU Affero General Public
// License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.

// alexandre1985.gitlab.io is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public
// License along with alexandre1985.gitlab.io. If not, see
// <https://www.gnu.org/licenses/>.


$o('form *[type=button]').addEventListener('click', function(evt) {

    evt.preventDefault()

    const alertMsg = evt.target.dataset.alert

    alert(alertMsg)

    const form = evt.target.closest('form')

    // fetches email from form's html "data-email" attribute
    const sendToEmail = form.dataset.email

    // change this accordingly or fit this selectors to your html form code
    const subject = form.querySelector('*[name=_subject]').value
    const name = form.querySelector('input[name=name]').value
    const text = form.querySelector('textarea[name=message]').value

    // const signatureTextElement = form.querySelector('input[name=sign-bool]')

    // if(signatureTextElement.checked) {

    //     const nl = '\r\n';  // new line separator for email body
    //     const message = text + nl + nl + '---' + nl + name

    // } else {

        const message = text

    // }

    const mailtoLink = 'mailto:' + sendToEmail + '?body=' + encodeURIComponent(message) + '&subject=' + encodeURIComponent(subject)

    // trigger mailto link
    window.open(mailtoLink, '_self')
});
