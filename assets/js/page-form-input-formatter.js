// Copyright (C) 2024 Daniel Cerqueira

// This file is part of alexandre1985.gitlab.io.

// alexandre1985.gitlab.io is free software: you can redistribute it
// and/or modify it under the terms of the GNU Affero General Public
// License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.

// alexandre1985.gitlab.io is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public
// License along with alexandre1985.gitlab.io. If not, see
// <https://www.gnu.org/licenses/>.


String.prototype.capitalize = function() {
    const text = this.valueOf()
    return text.charAt(0).toUpperCase() + text.substr(1)
}

String.prototype.capitalizeWords = function() {
    return this.valueOf().replace(/\w\S*/g, function(text) {
        return text.capitalize()
    })
}


const formNameField = $o('form input[name=name]')
const formSubjectField = $o('form input[name=_subject]')
const formTextareaField = $o('form textarea')




$o('form button[type=button]').addEventListener('mouseup', function(evt) {
    // Capitalize every word of a Name
    formNameField.value = formNameField.value.capitalizeWords()
    // Capitalize first word of a Subject
    formSubjectField.value = formSubjectField.value.capitalize()

});

[formTextareaField, formSubjectField].forEach(function(el) {
    el.addEventListener('keyup', function(evt) {

        // Capitalize first letter of Message Text
        evt.target.value.length < 5 ? evt.target.value = evt.target.value.capitalize() : 0

    })
});
