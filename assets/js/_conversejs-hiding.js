/// ConverseJS variables
const converseTargetPoint = $o('#hero').offsetHeight * 0.25
let conversejs = null


//// clear conversejs on page load
if(window.scrollY <= converseTargetPoint) {

    let converseToHide = setInterval(() => {
        
        if(!conversejs) {
            conversejs = $o('#conversejs')
        }
        
        if(conversejs) {
            conversejs.classList.add('hide-chatbox')
        }

    }, 300)

    setTimeout(() => clearInterval(converseToHide), 2000)
}


//// clear conversejs on scroll
window.addEventListener('scroll', function(e) {
    
    if(!conversejs) {
        conversejs = $o('#conversejs')
    }
    
    if(conversejs) {

        if(window.scrollY <= converseTargetPoint) {
            conversejs.classList.add('hide-chatbox')
        } else {
            conversejs.classList.remove('hide-chatbox')
        }

    }

}, {passive: true})
