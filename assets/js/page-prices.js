// Copyright (C) 2024 Daniel Cerqueira

// This file is part of alexandre1985.gitlab.io.

// alexandre1985.gitlab.io is free software: you can redistribute it
// and/or modify it under the terms of the GNU Affero General Public
// License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.

// alexandre1985.gitlab.io is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public
// License along with alexandre1985.gitlab.io. If not, see
// <https://www.gnu.org/licenses/>.


function roundUp(price, decimals) {
    return Math.round(price * Math.pow(10, decimals)) / Math.pow(10, decimals);
}

function updateBitcoinPrice(oneBitcoinToEuroRate) {

    const pricingItems = $a('.pricing .pricing-item');

    [...pricingItems].forEach(function(item) {

        const itemFiatPrice = item.querySelector('.fiat .currency-value').innerText

        const itemSatoshiPrice = Math.round((itemFiatPrice / oneBitcoinToEuroRate) * 100000000)

        const itemBitcoinEl = item.querySelector('.bitcoin a')

        const bitcoinPrice = itemSatoshiPrice * 0.00000001

        itemBitcoinEl.href += ('?amount=' + bitcoinPrice)

        const itemPriceValueEl = item.querySelector('.bitcoin .currency-value')

        const microBitcoinPrice = itemSatoshiPrice * 0.01

        itemPriceValueEl.innerText = roundUp(microBitcoinPrice, 2)

        itemPriceValueEl.classList.remove('currency-loading')

    });

}

function updateIOTAPrice(oneMiotaToEuroRate) {
    const pricingItems = $a('.pricing .pricing-item');

    [...pricingItems].forEach(function(item) {

        const itemFiatPrice = item.querySelector('.fiat .currency-value').innerText

        const itemPriceValueEl = item.querySelector('.iota .currency-value')

        itemPriceValueEl.innerText = roundUp(itemFiatPrice / oneMiotaToEuroRate, 3)

        itemPriceValueEl.classList.remove('currency-loading')

    });
}


function fetchUpdate() {
    // bitcoin
    fetch('https://blockchain.info/ticker').then(response => response.json()).then(json => updateBitcoinPrice(json.EUR['15m']))

    // iota
    //fetch('https://iota-price.herokuapp.com/').then(response => response.json()).then(json => updateIOTAPrice(json.euro))
}


document.addEventListener('DOMContentLoaded', function() {

    setInterval(fetchUpdate(), 300000)

});
