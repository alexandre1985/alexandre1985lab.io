---
layout: post
title: Como eu conheci a liberdade
date: 2020-05-20
category: experiencia-que-passou
keywords: hospital magalhães lemos, abuso de médicos, abuso de autoridade médica, psiquiatra hospital magalhães lemos porto, maltrato de psiquiátras
photo:
  filename: yellow-and-blue-macaw.jpg
  alt: papagaio com uma asa aberta. o pagagaio é bonito, azul e amarelo
  creator:
    name: Couleur
    site: https://www.pexels.com/@couleur-1208853
---

Quero contar a minha experiência com os centros médicos do Porto. Mais concretamente com uma experiência que foi ser internado num Hospital Psiquiátrico.
<!-- stop -->

Fui internado no Hospital Psiquiátrico Magalhães Lemos por dor de barriga.  
Os médicos receitaram drogas pesadas para a cabeça sem terem algum diagnóstico.  
Após ter vivido os piores estados de saúde da minha vida até então, os médicos fizeram-se utilizar do medo por eu deixar de tomar medicamentos, através de algumas pessoas da minha família e da sua pressão.

Voltou a acontecer um reinternamento, por ter ouvido uma voz.

Médicos disseram que eu era esquizofrénico (de maneira indirecta), mas aqui o Daniel sabe o que não é.
Os médicos continuam a querer dar-me medicamentos, que passou a ser injecção.

Alguns dos efeitos dos medicamentos são, a morte precoce. Muitas das drogas que dão aos doentes mentais tem efeitos devastadores na saúde da pessoa. Leiam o folheto (de Haldol, Invega, e semelhantes) online.

Não consegui tomar mais drogas legais (ironicamente os médicos as chamam de "medicamentos") e decidi deixá-las. Mais uma vez fui pressionado e rogaram-me pragas subtis por estar a tentar, passo a passo, realizar o meu processo de libertação de drogas; drogas essas legais (impingidas por profissionais da "medicina").

Ainda não foi essa a vez em que me libertei. Voltei a ser reinternado. Na altura não respirei profundamente por muito que tivesse tentado. Atirei-me para a frente de um autocarro por ter seguido ordens da voz no estado de bloqueio da respiração profunda. Tenho pena de mim por ter feito tal acto.

Médica psiquiátrica Andreia Norton acompanhada de outra médica, vai a tribunal a querer que eu fique a viver a minha vida dentro de centros de saúde como o Hospital Psiquiátrico Magalhães Lemos.  
Após o tribunal, e eu ainda dentro do Hospital Psiquiátrico Magalhães Lemos, vejo-me forçado, devido a estar encurralado, a assinar uma folha a dizer que não vou viver no Hospital Psiquiátrico Magalhães Lemos com a condição de tomar drogas pesadas e estúpidas que causaram escassez de saúde.

A policia tentou querer saber o meu paradeiro.

Sou pleno de saúde e livre de qualquer droga. Estou a pensar em começar a exercitar para estar ainda melhor de saúde.
