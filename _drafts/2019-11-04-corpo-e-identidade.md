---
layout: post
title: Identidade e o teu Corpo
date: 04-11-2019
category: pensamentos
keywords: hospitalizado, médicos, corpo e identidade
photo:
  filename: copo-de-agua.jpg
  alt: copo com água
  creator:
    name : Meir Roth
    site: https://www.pexels.com/@meirroth
---
Facto, interessante é que quando fui ao hospital para ver o que se *passava* comigo (talvez drogado dentro de casa por meu pai) os médicos disseram que iam trocar-me de corpo com outro homem (que é mais velho) mas perante a questão que coloquei recorreram a dizer que tinham de pôr uma coisa que ia-os fazer ouvir todos os meus pensamentos. Weird.

<!-- stop -->

Senti nada, foi tipo: já tá! (provavelmente fizeram tal numa dimensão à parte, não tenho conhecimento). A noção de tempo para mim foi sentida (parti de um ponto de tempo e voltei quase para o mesmo ponto).

Por isso, todos os meus pensamentos são transparentes para estes médicos (e a sua "equipa", que passa muito além de pessoas envolvidas em medicina)

Equipas é divisório. Os mesmo que tem *equipa* (para lutar contra) são e trabalham para "*união de todos*" (não parece que caminhem para o objectivo da união)

E a questão que coloquei aos médicos foi: Como é que os meus pais vão saber quem eu sou? (sem resposta, não executaram a mudança de corpo)

Ser transparente, para mim deriva muita das circunstâncias da minha vida.
