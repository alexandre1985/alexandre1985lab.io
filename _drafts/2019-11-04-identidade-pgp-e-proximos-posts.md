---
layout: post
title: Identidade PGP
date: 04-11-2019
category: tecnologia
keywords: pgp, password, identidade, git commits
photo:
  filename: internet-screen-security-protection.jpg
  alt: ecrã de computador na internet a clicar em botão de segurança
  creator:
    name : Pixabay
    site: https://www.pexels.com/@pixabay
---

Bem, resolvi desistir de usar PGP para "assegurar" identidade na Internet. Sempre há maneira de não ser a tua verdadeira identidade.  
<!-- stop -->
(opinião minha)

Talvez passe a desistir de qualquer género de identidade. Não sendo possivel (verdadeiro, pois a muitos websites requerer *contas de utilizador* de maneira obrigatória), continuarei nesta ilusão de a tua identidade está guarantida com passwords que acreditamos serem de só de conhecimento pessoal.

Desejo sorte.

##### [Nem o teu corpo serve de identidade]({{site.posts[-16].url}})

Os meu git commits não terão assinaturas gpg, daqui em frente.
