---
layout: post
title: Learn great Firefox search skills
date: 2019-12-14
photo: 
  filename: binocular.jpg
  alt: a pair of binoculars
  creator:
    name : Skitterphoto
    site: https://www.pexels.com/@skitterphoto
keywords: search skills, search engines, firefox
---
<!-- stop -->
{% include component/peertube-video.html video="https://p.eertu.be/videos/embed/229577ce-984d-40f2-9bff-5f123ccde0a3" video-page="https://p.eertu.be/w/5gGaYeAurMtmaqhZsthR5F" %}
