---
layout: post
title: Remove default dictionary from Sublime Text
date: 2019-12-20
categories: gnu
photo:
  filename: editor-de-codigo.jpg
  alt: computer screen with a code editor
  creator:
    name : Pixabay
    site: https://www.pexels.com/@pixabay
---

## Intro

This post is useful if you want to add custom dictionaries to your Sublime Text spell checking.

<!-- stop -->

## Remove default dictionary

```bash
rm "/opt/sublime_text/Packages/Language - English.sublime-package"
```

## Add dictionaries from the system (from hunspell)

1. create a Sublime Text dictionary folder

	```bash
	mkdir ~/.config/sublime-text-3/Packages/Dictionaries/
	```

2. add *hunspell*'s dictionary to this folder


	```bash
	# Portuguese
	ln -s /usr/share/hunspell/pt_PT.dic ~/.config/sublime-text-3/Packages/Dictionaries/pt_PT.dic
	ln -s /usr/share/hunspell/pt_PT.aff ~/.config/sublime-text-3/Packages/Dictionaries/pt_PT.aff
	# English
	ln -s /usr/share/hunspell/en_GB.dic ~/.config/sublime-text-3/Packages/Dictionaries/en_GB.dic
	ln -s /usr/share/hunspell/en_GB.aff ~/.config/sublime-text-3/Packages/Dictionaries/en_GB.aff
	```

3. add a predefined dictionary to Sublime.
	Go to `Preferences > Settings` and add the line

	```
	"dictionary": "Packages/Dictionaries/pt_PT.dic",
	```

Done!

Now, all the above dictionaries are in Sublime's Spell Checker (`View > Dictionary`).
