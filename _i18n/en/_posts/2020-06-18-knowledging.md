---
layout: post
title: Knowledging
date: 2020-06-18
photo: 
  filename: learning-board.jpg
  alt: young adult writing on blackboard
  creator:
    name: fauxels
    site: https://www.pexels.com/@fauxels
---

With Enlightment and its notion, came the Knowledge and its culture.

Knowledge is the honouring of a grasping mind :wink:
