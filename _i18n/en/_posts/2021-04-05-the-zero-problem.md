---
layout: post
title: The Zero Problem
date: 2021-04-05
categories: numbers
keywords: zero problem, the zero problem, zero fiasco, number zero, why zero, decimal zero
photo:
  filename: empty-metro.jpg
  alt: An empty metro, of course, with zero passengers
  creator:
    name : Pixabay
    site: https://www.pexels.com/@pixabay
---

Numeric systems have a specific domain. That domain is counting. So we
can say that a numeric system, such as the decimal system, serves for
counting purpose.

We can divide the world of things into 3 parts: the nothing, the
something, the everything.
In terms of the view of the world in quantities, the 3 part correlate
with: nothing, something, group.

These are concepts.

So, nothing is a concept, group is a concept and something is a concept.

Numeric systems regard only the something concept. That is the numeric
system domain.

## The zero

Someone invented the zero. That is ok. Zero, or nothing, is a concept.
For me, the problem came with the introduction of this zero into the numeric
system. Zero started being a number. Which is problematic in various
areas of mathematics.

## Problems raised by zero

In most areas where mathematics is used, zero creates problems. For
example, the monetary system; it makes no sense to create a coin that
correlates to nothing in value. For monetary systems, the base coin has
always been one, because it has a value (one cent, in case of euro).
The same applies for other areas of mathematics. It makes sense for the
base to be some number other than zero.

A problem that the introduction of zero as a number brings, is that the
second number is the number one, the third number is the number two, and
so on. This is confusing. All because zero is a number (it is the first
number).

## Solution

Use numeric systems that have the base point of 1. Reformulated the
decimal 0-based system to be a decimal 1-based system.
