---
layout: post
title: Conflict
date: 2020-07-10
photo: 
  filename: dry-desert.jpg
  alt: dry land with one leafless tree and dry soil
  creator:
    name : Pixabay
    site: https://www.pexels.com/@pixabay
---

Some people are being drawn into conflict.

When I turn on the news, or pay more close attention to what the intention of most discourses that I listen, conflict is the intention predominantly.

Since when I was a child, and I started imitating the 'super-hero', this started escalating, with the 'super-hero' having to fight something or someone, no matter how noble the cause appears to be.

Some people that say: 'we must *fight* this whatever (COVID-19?)', 'rise *above* (above who?)', 'let not *escape* this opportunity (such as every other opportunity that keeps on appearing?)'.

Watch out. Tell me not what conflict has done to myself, but what have I become with conflict (situations).

