---
layout: post
title: My English Blog
date: 2018-01-31
photo:
  filename: london-city.jpg
  alt: london city with a bridge
  creator:
    name : Pixabay
    site: https://www.pexels.com/@pixabay
---
Currently I'm not planning writing my blog in English. So to read my posts please go to [my Blog in Portuguese](/blog/).
