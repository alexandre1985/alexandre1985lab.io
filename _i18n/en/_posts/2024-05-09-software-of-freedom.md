---
layout: post
title: Software of Freedom
date: 2024-05-09
categories: gnu
keywords: free software, freedom, software of freedom, gnu, linux, gnu/linux, gnu+linux
photo:
  filename: i-love-fs.png
  alt: GNU logo and a girl with some headphones
  creator:
    name : Carlos Chan
  site:
    name: GNU.org
    site: https://www.gnu.org/graphics/chan-i-love-fs-banner.html
---
I will tell you what Free Software is, in simple terms.
<!-- stop -->

Most Free Software people, when trying to reply to the question "What is Free Software?", cannot tell a simple, concrete answer to this question. So, we can say that the person asking will not get a clue about what Free Software is.

The answers (that Free Software people give) get easily carry away to other spheres of the world, not related to Software. So, I will try to give an concrete answer to the question above.

## What is Free Software?

Free Software is a Software in which its *source code* has freedom.

## What is Source Code?

Source code is a text file (or many text files) which is written, by a programmer, in a computer languages. There are many computer languages, so a source code can be in various different languages.

## What is a source code that has freedom?

A source code that has freedom, is a source code that:

 - you can share as you may wish.
 - you can read and edit as you may wish.
 - you can share it edited, as you may wish.
 - no one can remove any of the above rights from a source code of freedom.

## Proprietary source code

You may think that it is obvious that all *software* is Free Software. But that is not the case.

Some computer languages need the source code to be *compiled*, into a file, in order to execute it. And this compiled file is **very very very** difficult to read. We can say that the compiled file is illegible. The compiled file was compiled to *machine language*, basically a bunch of zeros and ones.

In the early history of software, all programmers shared the source code of the programs, in order for the other party to use, edit, read, and execute, the program. After the early history of software, some companies and some individuals, started to give away only the compiled version of a program to others, keeping the source code for themselves.

And, by keeping the source code for themselves, these programmers gain power over others, because they were the only ones who could know what the program did, and they were the only ones who could edit the program. This *power over others* is a nasty weapon upon a user, that needs certain program for some daily activity.

## The malevolence of proprietary Software

Here are some of the nasty problems that this *proprietary source code* can produce:

 - Hidden backdoors that remote control the person's computer that execute such code.
 - Hidden collection and transfering of computer's data from the person's computer that execute such code.
 - Intentional damaging of the computer's software executing such code.
 - Limiting the available features of the program.
 - Anti-Features made to annoy, control, the person's executing such code.
 - Many other nasty stuff.

## Free source code

So, Richard Stallman, seeing *proprietary software* rising, decided to create the definition what a *Free Software* is, and he started the **Free Software movement**, to try to abolish *proprietary software* and its nastiness.

Here are some main websites about Free Software:

 - [Free Software Foundation](https://www.fsf.org/)
 - [GNU operating system](https://www.gnu.org/)
 - [LibrePlanet](https://libreplanet.org/)
 - [H-Node](https://h-node.org/)
