---
layout: post
title: Bitcoin Keyboard Layout
date: 2021-04-02
categories: gnu
keywords: custom keyboard layout, new keyboard layout, x11, xkb, gnu, linux
photo:
  filename: bitcoin.jpg
  alt: a metal bitcoin coin
  creator:
    name : Moose Photos
    site: https://www.pexels.com/@moose-photos-170195
---
For some time, I have been using a custom Portuguese keyboard layout on
my Arch Linux machine. In fact, there are two machines.

I want to share now, how I have made a new custom keyboard layout (I
will be using the Portuguese keyboard layout as the base point and you
can choose whatever base keyboard layout you like).

## Creating a new keyboard on GNU X11 graphical environment

First, let's name the new keyboard layout variant. I am naming it "daniel" and giving the description "Portuguese
(Daniel)".

Edit the file `/usr/share/X11/xkb/rules/base.lst` and add the following
line:
```
...
  daniel          pt: Portuguese (Daniel)
...
```
and also edit the `/usr/share/X11/xkb/rules/base.xml` file, with:
```
...
<variant>
  <configItem>
    <name>daniel</name>
    <description>Portuguese (Daniel)</description>
  </configItem>
</variant>
...
```
now the new keyboard variant it set up to be recognized.

All that is left to do it to create the custom keyboard symbol mapping.
For this to happen, the Portuguese keyboard symbol file `/usr/share/X11/xkb/symbols/pt` needs to be edited with a new Portuguese variant.

I can give an example of swapping the Portuguese slash key with the
backslash key. For this to happen you need to add this section:
```
...
xkb_symbols "daniel" {
    include "pt(basic)"
	 key <TLDE> { [  slash, bar, notsign, notsign  ] };
	 key <AE07> { [  7, backslash, rightdoublequotemark, seveneighths  ] };
}
...
```
you can add and modify as many lines as you want.

As a side note, here is my current Portuguese keyboard variant:
```
//
// Portuguese Daniel's Keyboard
//
partial alphanumeric_keys
xkb_symbols "daniel" {
    include "pt(basic)"

    key <TLDE> { [         slash,             bar,        notsign,          notsign ] };

    key <AE07> { [             7,           plus,rightdoublequotemark, seveneighths ] };
    key <AE08> { [             8,        asterisk, dead_diaeresis,        trademark ] };
    key <AE09> { [             9,       parenleft,   dead_cedilla,        plusminus ] };

    key <AE11> { [    apostrophe,        question,           less,     questiondown ] };
    key <AE12> { [ guillemotleft,  guillemotright,        greater,      dead_ogonek ] };

    key <AD11> { [     braceleft,     bracketleft,    ordfeminine,    dead_ordering ] };
    key <AC11> { [    braceright,    bracketright,      masculine,       dead_caron ] };

    key <LSGT> { [     parenleft,      parenright,      backslash,        backslash ] };
    key <AB05> { [             b,               B,      U20BF, rightsinglequotemark ] };
};

```
note the last line, set's my Alt+B to the bitcoin symbol, ₿, by using
the unicode character U20BF.

Now the new keyboard variant is set in your GNU system and what is left to do is to go to the keyboard system settings and choose the keyboard variant "Portuguese (Daniel)".
