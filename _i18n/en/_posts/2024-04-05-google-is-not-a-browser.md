---
layout: post
title: Google is not a Browser
date: 2024-04-05
categories: web
keywords: google is not, google, search engine, web browser, browser
photo:
  filename: globe-map.jpg
  alt: a globe, which is an spherical map of the world
  creator:
    name : NastyaSensei
    site: https://www.instagram.com/nastya_sens_/
---
Many people are mixing the meaning of *web browser* and *search engine*.
<!-- stop -->

## Today's scenario

I am hearing more and more people having the confusion about what *search engine* and *web browser* are. Although these two work together, these are two completely different things.

Since Android has come to the mainstream, and more and more people started using this operativing system, people got exposed to some "apps" that are search engines, such as the "Google" *app*. Google app is an Android application which does searches at the Google *search engine* and also acts as a funky *web browser*, that does not accept URLs as input (only accepts *web searches*). So things got more tied together and confusion starts to settle in.

## Definitions

A *web browser* is an program that let's you view the Web.

*Web* is a network where web pages live. Is a subset of the Internet.

A *search engine* is a website that has gathered information about many web pages, and offers the service of showing a result of web pages (from their gathered information) that more closely match a *search query*.

A *search query* is the information, related to a *web search*, that is sent to the *search engine*. It is more commonly composed by *search keywords*, and can also have more information, such as country, date, etc..

## Google

Google is the company of software that has created the google.com website, which is a website to do *web searches* (google.com is a "search engine").

Google Chrome is a *web browser* that Google has forked, from Chromium (which is another *web browser*). Firefox is also a *web browser* (from Mozilla).

So Google (as in google.com) and Google Chrome gets confused sometimes, but these are two different things. Google Chrome is a *web browser*, Google is the company owner of google.com. And google.com is a *search engine*.

## There are many

There are many *search engines* services besides what Google offers. Some of them are: Searx, Bing, Yahoo, Mojeek, Qwant, DuckDuckGo, etc..

There are many *web browsers*, besides Google Chrome. Some of them are: Firefox, Icecat, Tor Browser, Nyxt, Chromium, Qutebrowser, etc..

## Last piece of information

A *web browser* may have many *search engines* configured.

Google Chrome may do *web searches* on other *search engines*, besides its default one, which is google.com.

And google.com may be configured on most *web browsers* to perform the web browser's *web searches*.
