---
layout: post
title: Super-Unit
date: 2024-05-25
categories: numbers
keywords: number, super-unit, unit
photo:
  filename: truck.jpg
  alt: image alternate text
  creator:
    name : Maarten van den Heuvel
    site: https://www.pexels.com/@mvdheuvel/
---
*Unit*, or the *Point*, is the basic concept of numbers. It is also the basic concept is logic and in various other areas (that I am not remembering right now). In order for some branch of knowleadge to begin, most of the time, it has to start with a unit. Or a fundamental concept (which is a unit).
<!-- stop -->

In numbers, the unit is the number 1.

If we want to go below the unit, there is fractions (or floating-point numbers), if we want to go up, there is the numbers above 1.

Sometimes we want to start making reference for some number above 1, as a unit. That number above 1, is an example of a **super-unit**.

For example: in a factory of computer RAM memory, RAM memory is produced by the unit. And, after the RAM is created, it is packaged into a package of 4 memory RAM sticks, in order to be sold. Instead of saying "a selling package of 4 RAM units" you may define this selling package to be a *super-unit*, saying only the "selling package". This *selling package* is a super-unit (of 4 units). Now, the factory needs to ship this *selling package* in its trucks. The trucks can only carry 99 *selling packages*. The factory may define another *super-unit* and call it "truck" that carries 99 super-units (*selling package*) of 4 units (*RAM*), or just saying that it carries 99 *selling packages*, since the name "selling package" already is a defined super-unit.

So this *truck* is also another *super-unit*. More specifically, the *truck* it is a *super-unit* of another *super-unit*. Which is simply called just as a *super-unit*.

A *super-unit* can be any number above 1, which is defined to suit a context, and simplify wording.

So now there is fractions to define something below the unit, and super-unit to define something above the unit.
