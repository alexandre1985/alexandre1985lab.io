---
layout: post
title: Imi, o conceito lógico antigo (sempre apagado e agora renascido)
date: 30-10-2019
category: fala
keywords: imi, trindade, pensamento lógico
photo:
  filename: ancient-architecture-asia-buddhism.jpg
  alt: arquitectura antiga na tailândia
  creator:
    name : Pixabay
    site: https://www.pexels.com/@pixabay
---

Imi, é um atributo final do pensamento lógico. *Atributo final* só existem 2 no pensamento lógico que recebi na minha educação (escolar), que são o *falso* e o *verdadeiro*; *imi* é o terceiro, estabelecido de hoje em diante.

<!-- stop -->

## O que é?

O significado de *imi* na lógica, é o mesmo que o número zero nos números, e a igualdade na matemática e suas equações. O ponto neutro, no mundo do movimento (os carros podem andar para a frente - acelaração normal - e para trás - marcha atrás - ou andar no ponto neutro - sem fazer aquilo que os causa andar: que é o acelarar).

Bem... Entendido este conceito, e percebendo bem esta analogia dos carros, *imi* fica entendido :wink:

*Imi* é o mesmo que a propriedade de neutro, na Química. Assim entende-se melhor.

## Imi, na programação

É o mesmo que o conceito de *null* ou *nil* (algumas línguas, tal como o JavaScript, usam o *null* mais o *undefined*, que no mundo da lógica *undefinied* e *null* são dois atributos com significado pouquíssimo diferenciado).

## Conclusão

No mundo da lógica estamos a categorizar, e com o *imi* a frase/verso pode ser ou *verdadeira* ou *falsa* ou *imi* (nem verdadeiro nem falso).
