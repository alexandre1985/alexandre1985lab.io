---
layout: post
title: Minha Nova Maneira de Lidar com Emoções Negativas
date: 03-02-2018
categories: espiritual
keywords: meditação diferente, aprofundar, emoções
photo:
  filename: deep-dive.jpg
  alt: mulher a nadar debaixo da água
  creator:
    name : Elaine Bernadine Castro
    site: https://www.pexels.com/@elaine-bernadine-castro-1263177
---
Pequena revelação sobre mim. Tenho andado com alguma curiosidade sobre a espiritualidade da Índia Antiga. É simplesmente fantástico o conhecimento que veio desta terra sobre o ser humano e o mundo em que vive. Mundo que não é apenas o material como associamos aqui no ocidente quando falamos a palavra "*mundo*". Vai além do material; e essa é a parte que mais me interessa, já que o material está bastante bem estudado aqui no Ocidente e, também, não sou o melhor explorador, nem de perto nem de longe, do mundo material que existe no dia de hoje.  
<!-- stop -->
Esta curiosidade está a resultar no estudo da meditação. E hoje tive uma pequenina revelação que quero partilhar.  

Na maioria das meditações que ouço falar, pelo menos a parte inicial, consiste em deixar pensamentos e emoções, que surgem quando meditando, entrarem. Depois deixa-se esses pensamentos e emoções existirem (dentro de ti)... sem nos agarrarmos a eles. O que acontece é que se não nos agarramos a eles, eles eventualmente saíram "de dentro de ti" e continuaram a sua viagem pelo mundo :dizzy: .  
A parte que acho mais difícil é não me agarrar às emoções que aceito abertamente dentro de mim. Isto porque a minha mentalidade é mais de mudar o mundo (para melhor?) em vez de deixá-lo estar como é, ou aceitá-lo como ele é. É a minha maneira de ser mais predominante.  
Logo hoje descobri uma maneira obter o mesmo efeito, utilizando esta minha característica mais predominante... E isto não li isto em parte alguma.  
Vou contar o que aconteceu-me hoje explicando o meu agir para entenderem o método (que melhor se adequa a mim) que descobri, que acho que vai dar mais frutos do que "não agarrar às emoções que entram dentro de ti".  

Então hoje estava a tomar chá num café com o meu pai. Passado algum tempo olho para uma mesa que está à minha frente no fundo e vejo que está lá um amigo meu de infância. Eu entrei e não o cumprimentei nem falei com ele (sem querer). E até gostava muito de o ter feito mas pelos vistos, eu sou distraído e muitas vezes não reparo nas pessoas à minha volta.  
Acontece que a partir do momento em que eu o reconheci, não há uma boa chance para lhe acenar sem ser corta o ambiente. Queria dizer que o reconheci mas sem ser muito brusco em relação a isso. O momento acontece quando ele vai pagar ao balcão. Eu sabia que quando ele saísse do balcão de pagamento e ao dirigir-se para a porta que ele podia virar a cabeça para a minha mesa e aí eu ia expressar o meu reconhecimento por ele (que já faz muito tempo desde que o vi). Acontece que quando ele sai do pagamento e dirige-se para a saída ele passa o tempo todo a olhar para o lado contrário e com a cabeça levantada um pouco acima da normalidade. Eventualmente estaria a ignorar-me e suponho que seja por estar chateado de eu não o ter cumprimentado. E eu tenho uma boa impressão que ele me tinha visto e reconhecido, pela maneira que o amigo dele olha para mim de vez em quando durante a conversa que estavam a ter (antes do pagamento).  
Portanto esse ignorar (aparente para mim e interpretado dessa maneira por mim) fez-me sentir que não tenho valor. E esse sentimento tornou-se grande demais, dentro de mim, para o conseguir conter... O que eu costumava fazer nesta altura era, ignorar o sentimento de inferioridade (pensar que o meu amigo é que perde) em vez de aceitar esse sentimento. E o que fiz foi mesmo aceitar o sentimento, mesmo sabendo que não ia trazer uma sensação positiva em mim.  
Ao aceitar o sentimento, em vez de não me agarrar a ele (não conseguia, o sentimento estava-se a tornar demasiado grande) decidi **mergulhar a fundo** no sentimento. Experienciá-lo *totalmente*. E acontece que após eu mergulhar nesse sentimento e concentrar-me totalmente nele, isto fez com que o sentimento se desvanecesse.  
Não fiquei com a sensação de inferioridade que talvez ficasse se tivesse ignorado ou tivesse levado a peito sem me *concentrar totalmente* no sentimento. Experienciei o sentimento totalmente e acontece que ele era feito de nada... o sentimento mostrou-se insignificante.  
(embora no passado tenha pensado que este sentimento trazido por este evento fosse negativo para mim, fosse *sentimento **não desejado***).  
Gosto deste método :smile:
