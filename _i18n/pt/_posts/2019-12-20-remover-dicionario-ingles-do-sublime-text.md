---
layout: post
title: Remover do Sublime Text dicionários Inglês
date: 2019-12-20
categories: tecnologia
keywords: dicionário no sublime text, sublime text português
photo:
  filename: editor-de-codigo.jpg
  alt: ecrã de computador com editor de código
  creator:
    name : Pixabay
    site: https://www.pexels.com/@pixabay
---

O Sublime Text vêm com alguns dicionários da língua Inglesa, por omissão.

Para os remover, abre um terminal e corre o comando:

<!-- stop -->

```bash
rm "/opt/sublime_text/Packages/Language - English.sublime-package"
```

Já está!

Caso queiras adicionar alguns dicionários (por exemplo, o Português) ao *Corrector de Erros* do Sublime Text, basta teres um Corrector Gramatical no sistema (por exemplo, o *hunspell*) e:

1. criares uma pasta nas configurações do Sublime:

	```bash
	mkdir ~/.config/sublime-text-3/Packages/Dictionaries/
	```

2. adicionares os dicionários do *hunspell* a esta pasta


	```bash
	# Português
	ln -s /usr/share/hunspell/pt_PT.dic ~/.config/sublime-text-3/Packages/Dictionaries/pt_PT.dic
	ln -s /usr/share/hunspell/pt_PT.aff ~/.config/sublime-text-3/Packages/Dictionaries/pt_PT.aff
	# Inglês
	ln -s /usr/share/hunspell/en_GB.dic ~/.config/sublime-text-3/Packages/Dictionaries/en_GB.dic
	ln -s /usr/share/hunspell/en_GB.aff ~/.config/sublime-text-3/Packages/Dictionaries/en_GB.aff
	```

3. adicionares o dicionário predefinido às configurações do Sublime Text.  
	Para tal vais, dentro do Sublime, a `Preferences > Settings` e adicionas a linha

	```
	"dictionary": "Packages/Dictionaries/pt_PT.dic",
	```

Feito.

Poderás ver os dicionários adicionados em `View > Dictionary` dentro do Sublime Text.
