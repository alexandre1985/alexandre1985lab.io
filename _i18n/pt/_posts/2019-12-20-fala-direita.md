---
layout: post
title: Fala direita
date: 2019-12-20
keywords: língua portuguesa correcta, fala direita, fala correcta
categories: fala
photo:
  filename: estatua-pequeno-budista.jpg
  alt: estátua branca de pequeno budista
  creator:
    name : Mike
    site: https://www.pexels.com/@mikebirdy
---
Esta é o meu presente à língua Portuguesa. A melhor obra que consegui fazer até hoje. Tem apenas uma página. Tens de ler.... mesmo.

<!-- stop -->

<div class="text-center">
	<a class="btn btn-outline-info" href="/assets/other/fala-direita.pdf">
		<span class="file-pdf">
			&nbsp;Fala direita
		</span>
	</a>
</div>

Agradecido :grinning: :gift_heart:
