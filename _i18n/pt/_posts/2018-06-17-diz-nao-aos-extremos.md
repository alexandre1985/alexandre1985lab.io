---
layout: post
title: Diz não aos extremos
date: 2018-06-17
category: pensamentos
keywords: extremo, paradoxo, relatividade, positivo e negativo
photo:
  filename: photo-night-sky.jpg
  alt: foto do céu à noite
  creator:
    name : Faaiq Ackmerd
    site: https://www.pexels.com/@faaiq-ackmerd-383634
---
A vida é cheia de paradoxos porque é necessário conciliar ambos os extremos do paradoxo para atingir o absoluto.  
O problema só surge quando se vê a vida relativamente, criando assim a possibilidade de existência de paradoxos.  
<!-- stop -->
##### Passo a explicar o porquê.  
Quando algo é relativo estamos a usar um ponto ou coisa como referência.  
Assim criamos uma escala e categorizamos as coisas como estando de um lado ou do outro da escala, nascendo assim o positivo e o negativo.  
O paradoxo é algo que é contrário a outro, isto é, um lado do paradoxo é o positivo e o outro o negativo.  
Logo o paradoxo só existe quando há relatividade.  

(Dammn :smiley:)
