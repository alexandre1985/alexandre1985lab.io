---
layout: post
title: Firefox e motores de busca
date: 2019-12-16
category: tecnologia
keywords: atalhos do firefox, motores de busca
photo:
  filename: binocular.jpg
  alt: um par de binóculos
  creator:
    name : Skitterphoto
    site: https://www.pexels.com/@skitterphoto
---
<!-- stop -->
{% include component/peertube-video.html video="https://p.eertu.be/videos/embed/229577ce-984d-40f2-9bff-5f123ccde0a3" video-page="https://p.eertu.be/w/5gGaYeAurMtmaqhZsthR5F" %}
&nbsp;

*(Este vídeo está em Inglês)*
