---
layout: post
date: 2018-02-23
title: Falha de Segurança na fala de Portugal
categories: fala
keywords: negação do vazio, língua portuguesa
photo:
  filename: lady-on-the-phone.jpg
  alt: mulher a falar ao telemóvel
  creator:
    name : Kaboompics .com
    site: https://www.pexels.com/@kaboompics
---
É isso mesmo... Falha de segurança na fala Portuguesa, interessante ah?
<!-- stop -->
Vou-me explicar melhor. Com falha de segurança, eu quero dizer que descobri uma expressão que utilizamos em Portugal que logicamente não corresponde ao que queremos dizer. Fiz um PDF a explicar esta minha 'tese' (clicar botão abaixo). Chamei este conceito de *Negação do vazio*.

<div class="text-center">
    <a class="btn btn-outline-info" href="/assets/other/negacao-do-vazio.pdf">
        <span class="file-pdf">
            &nbsp;Negação do vazio
        </span>
    </a>
</div>
