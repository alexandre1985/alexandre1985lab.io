---
layout: post
title: "Operadores da fala: e"
date: 2020-03-07
category: fala
keywords: operador e, fala, falha na abordagem lógíca do operador e, falha na abordagem lógíca do operador ou
photo:
  filename: guys-chatting.jpg
  alt: 3 homens a conversar amigávelmente, enquanto bebem
  creator:
    name: Pressmaster
    site: https://www.pexels.com/@pressmaster
---

Vou falar sobre o operador da fala *e*.
<!-- stop -->

O teu sobrinho faz anos e diz querer uma camisola preta *e* branca.

Se deres uma camisola de cor totalmente branca, não estarás a cumprir o desejo. Se deres uma camisola de cor totalmente preta, também não.

Agora, imagina que o teu sobrinho quer uma camisola preta.

Se deres uma camisola totalmente branca, não estarás a cumprir o seu desejo. Mas se deres uma camisola com riscas pretas *e* brancas, não estarás a dar uma camisola preta? (com o acréscimo de ter branco)

Há quem considere que estás a dar uma camisola com a cor preta.

Mas na realidade, não é esse o desejo do teu sobrinho.

Tudo tem a haver com a maneira como a veracidade do *operador e* é vista.

### Visão da escola sobre a veracidade de operador 'e'

Há quem considere uma frase com o *e* é verdadeira quando todas as preposições do *e* (isto é, as pequenas sub-frases entre o *e*) são verdadeiras, uma após outra. Caso alguma dessas preposições, seja falsa, dá-se a veracidade como falsa.

Isto é, o *e* continua a verificar a veracidade até uma ser falsa. Caso chegue ao fim (das preposições) considera-se verdadeira.

Da mesma maneira, o *ou* continua a verificar a veracidade até uma ser verdadeira. Caso chegue ao fim, considera-se falsa.

Esta é a visão de algumas pessoas dentro do ramo da programação e pessoas eruditas.

Mas de facto, como podemos ver no exemplo das camisolas desejadas pelo sobrinho, esta levaria a que o desejo de uma camisola preta, com uma prenda dada de uma camisola preta e branca, fosse verdadeira. Fosse desejo concluído.

Na verdade, não é desejo concluído, e terás um sobrinho não satisfeito com a tua prenda.

Isto pede para uma nova abordagem sobre *e* e *ou*; sobre junção de preposição.

Na próxima publicação de blog falarei sobre esta nova abordagem de junção de preposição.
