---
layout: post
title: Nix, a possível futura nova palavra de Portugal
date: 2019-02-05
categories: fala
keywords: nix, bué, nova palavra portuguesa
photo:
  filename: group-of-people.jpg
  alt: grupo de pessoas reunidas com o sol no fundo a brilhar
  creator:
    name : Helena Lopes
    site: https://www.pexels.com/@wildlittlethingsphoto
---

## Resumo
Nix é uma palavra cuja proposta é ser contrária ao bué ou tótil. Utiliza-se da mesma maneira que o bué mas tem significado oposto.  

<!-- stop -->

Por exemplo:  
"Depois de 2008, pessoas ficaram com nix dinheiro e bué dívida, e ambos só tem vindo a aumentar".  
Aqui ao dizermos "só tem vindo a aumentar" estamos a referir quer ao aumento da dívida (isto é, mais bué = a aumentar) quer à diminuição do dinheiro dos portugueses (isto é, mais nix = a diminuir cada vez mais).
<br><br>


## Explicação alargada
O *Nix* nasce da tentativa de criar uma palavra que equilibre o *bué*.  
O *bué* nasce de uma geração que viu crescer muita coisa: marcas estrangeiras, as modas (tázos, diablos, lasers, roupa diferente), os telemóveis, a Internet...  
O *nix* nasce da mesma geração, que está a ver muita coisa retirada de si: poder de compra, alguns direitos e emprego. Digamos que é uma proposta de equilíbrio; se houve necessidade de surgir o *bué* penso que há necessidade de existir o *nix*.

#### Porquê *Nix*?
*Nix* é uma palavra que, para quem não conheça e a ouça numa frase, o seu significado pode ser facilmente deduzido sem perguntar "o que é que queres dizer com nix?". É intuitivo que significa pouco, muito pouco ou até nada. Tal como *bué* significa muito ou muito muito :laughing: (eu sei que *muito muito* não existe).

#### Origem
Esta ideia *fantástica* (talvez) veio após observar a língua inglesa, nomeadamente o sufixo *less* e *ful*.  
Estes sufixos podem-se aplicar a qualquer palavra e simplificam a fala. Por exemplo, pode-se dizer *mindless* aplicado a uma pessoa, normalmente significa que essa pessoa tem pouco raciocínio, ou pode-se dizer a uma pessoa que ela é *mindful*, que significa que a pessoa é muito consciente (muita mente). Aqui o *mind* é o objecto do nosso sufixo, e o *less* indica que a *mind* (mente) está abaixo da normalidade, e o *ful* acima da normalidade. (estes significados também são contextuais).  
Estes sufixos, aplicados a qualquer palavra na língua inglesa tornam-a mais fácil de falar (e entender). Para além de tornar a língua mais à base de conceitos (conceitual).  
Ao pensar neste assunto reparei que nós já temos este conceito introduzido na nossa língua, mas não introduzido como sufixos às palavras. Foi introduzido como a palavra *bué* (que corresponde ao sufixo inglês *ful*).  
A diferença é que *bué* não se aplica como sufixo ou prefixo, mas sim como uma palavra antes ou depois do *objecto da nossa quantificação ou qualificação*.  

#### É uma questão de equilíbrio
Logo, com o nascimento do *bué*, penso que existe um certo desequilíbrio no nosso querido Português. Ficamos a poder dizer muito de uma coisa; e não arranjamos uma correspondência para dizer pouco sobre essa coisa. Dizemos *bué fixe* ou *tá bué* para dizer *muito fixe* ou *melhor* mas não temos uma palavra para dizer *fixe mas do lado contrário do bué*; para além de *pouco fixe*.  
Dizemos muito, muito muito; queremos *mais* isto *mais* aquilo. E, também tenho a opinião, que o *pouco* também tem a sua mais valia (e importância) na nossa vida.  
Para além do mais, *pouco fixe* tem seu equivalente em *muito fixe* por isso não tem correspondência para *bué*. Precisamos de uma palavra que signifique *bué ao contrário*.  
Por isso, a minha proposta é a palavra *nix*, que podemos utilizar quando queremos referirmos ao conceito contrário do *bué*, no mesmo sítio da frase em que utilizamos o *bué*. A minha imaginação às vezes dá-me isto, e vejo o Português mais rico com uma palavra como esta :monkey_face: Gosto de inventar cenas... e torna mais jovem o Português (e o falante :yum:).  

Vamos passar aos exemplos

#### Exemplos e seus significados
"Está *nix* o tempo" ==> Está mau tempo  
"*Nix* T.P.C." ==> Não tenho trabalhos para casa (da escola)  
"Eu bebo *nix* cerveja" ==> Não bebo cerveja  
"Eu tenho muita família, mas irmãs: *nix*" / "Eu tenho muita família mas *nix* irmãs" / "Eu tenho muita família, *nix* irmãs" ==> Tenho muita família mas não tenho irmãs  
"A divisão do futebol traz *nix* civilidade" ==> A divisão que o futebol traz reduz-nos civilmente  
"Ontem acordei *nix*" ==> Ontem acordei sem energia  
"Amanhã vou escrever *nix* para o meu blog" ==> Amanhã não vou escrever para o meu blog

#### Finalizar
Espero que esta ideia do *nix* seja bem-vinda. Se leste até aqui estás de parabéns, e espero que eu tenha conseguido transmitir a minha ideia correctamente.

Estou aberto a feedback da vossa parte.

Partilhem este post e esta ideia com os vossos amigos(as) e talvez o *nix* seja parte do nosso Português actual.  

>Ser jovem tem *nix* a haver com a idade &nbsp;:wink:

Até já!
<br><br>
*__Nota:__ Se quiseres saber outra descoberta do português interessante aconselho vivamente a pequena leitura de esta [minha publicação de blog]({{site.posts[-4].url}})*.
