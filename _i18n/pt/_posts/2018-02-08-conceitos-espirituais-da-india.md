---
layout: post
title: Resumo de Conceitos Espirituais da Índia
date: 2018-02-08
categories: espiritual
keywords: índia, conceitos espirituais, budismo, hinduísmo
photo:
  filename: ganesha.jpg
  alt: estátua de Ganesha
  creator:
    name : Artem Beliaikin
    site: https://www.pexels.com/@belart84
---
Hoje foi um dia especial para mim.
<!-- stop -->
No fim do meu dia descubro uma série de vídeos que explicam os conceitos espirituais da Índia, algo que há algum tempo andava à procura.
Isto porque tenho andado a investigar e estudar textos da Índia e é difícil perceber todos os conceitos descritos nele para alguém que não nasceu dentro dessa cultura, nesta vida. Por isso é que procurava um texto (apareceu na forma de vídeo) que desse uma visão geral dos conceitos espirituais da Índia, para melhor entender os textos que estou a ler.
Vou partilhar um vídeo que explica os conceitos gerais da espiritualidade Indiana.
&nbsp;

### Introdução ao Hinduísmo

{% include component/youtube-video.html video="WhTpJxlJi2I" %}

&nbsp;
### Outros vídeos relacionados
Partilho também a playlist do youtube de onde o último vídeo veio. Tem outros 7 vídeos importantes que são a continuação dos conceitos e da história da espiritualidade da Índia e da Ásia Oriental. Muito interessante. (Também fala de Budismo).
[Playlist da Khan Academy sobre Hinduísmo e Budismo](https://yewtu.be/watch?v=INv2gdpfXPQ&list=PLSQl0a2vh4HB9UeibLURBlcdR4XzputM9&index=44)

Salut
