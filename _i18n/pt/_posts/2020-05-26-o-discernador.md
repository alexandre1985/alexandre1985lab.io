---
layout: post
title: Discernador e o Poder/Controlo
date: 2020-05-26
keywords: o discernador, guerra com a terra água ar, apoiantes do discernador, terra água ar
photo:
  filename: river-beautiful.jpg
  alt: um rio sereno com muitas montanhas altas e pouco largas, com um homem em cima de um barco para poucas pessoas, um barco pequeno de pescador asiático
  creator:
    name: Quang Nguyen Vinh
    site: https://www.pexels.com/@quang-nguyen-vinh-222549
---

O planeta Terra dá tudo que humanos querem e precisam. Este post mostra porque podes não concordar com a frase anterior.
<!-- stop -->

A Terra nasce e dá ao Humano tudo o que o Humano quer. E o Humano encontrou o que deseja.
Dar a Terra faz.
Nasce o Homem, e os seus desejos têm casa.
Na Terra, algo cresce, reproduz, mantém, transforma, não se perde.
Até que, num momento infeliz, nasce o Discernador. Algo é igual, mas Discernador não gosta disso.
Discernador procura convencer todos os Algo de serem diferentes. Arranjou meios para realizar esse plano, e o Poder nasceu.
O Discernador começou a acumular, à vista de todos e alguns estranharam o porquê, alguns ignoram, alguns apoioram e todos deixaram.
O Discernador dividiu as mentes dos que o apoiaram, com vista a crescer-lhes o ódio, através de mentiras e falsas percepções. E os que o apoiaram acreditaram, divididos, tornando-se apoiantes. Assim o Discernador ganhou Poder.
O plano está a ser revelado e consiste em destruir tudo o que é Livre, isto é, os algos que dão sem discriminação e dão a todos.
O Discernador, fazendo uso do Poder, exerce-o sempre através do acumular, através do diferenciar. O Discernador ficou o maior de todos os algos, e os apoiantes do Discernador Puderam ser os segundos maiores, pois o Discernador partilha com os seus apoiantes pouco do seu Poder.
A partilha do Poder pelo Discernador não é dar, pois o Discernador quer destruir quem dá. Os apoiantes não sabem disso, e pensam o oposto, e assim acreditam na mentira de que o Discernador dá. Alguns apoiantes apercebem-se, com o tempo, que o apoio que davam ao Discernador, não os tornava maiores, que traz infelicidade que não tinham antes, que os destrói e destrói o que é Livre, pois a Terra dá mais do que o Discernador alguma vez conseguiu-lhes dar.
E esses alguns tornaram-se sóbrios.
O Discernador teme esses sóbrios, pois esses sabem o que é mentira. E o Discernador chama-os de insanos, virando-se contra estes sóbrios (antigos apoiantes), também.
O Discernador discerna, e o 'in' o 'des' são as ferramentas de batalha do Discernador, daí a palavra insanos, daí o **não** sanos.
Com o número crescente de apoiantes, o número preciso é atingido para batalhar. E a batalha foi a Terra.
Com algum tempo a batalha foi ganha pelo Discernador, e todos os Humanos passaram a ter de ter permissão do Discernador para terem um pedaço da Terra; todos os Humanos passaram a ter de pagar para terem pedacinhos de Terra.
Depois da batalha pela Terra ter sido "ganha", o Discernador descobriu, mais, algo que dá livremente. Descobriu a Água.
O Discernador criou um novo plano de batalha, a batalha pela Água.
Assim, o Discernador disse, a Água **não** é potável. E envenenou alguma Água de maneira a suportar essa mentira.  Esta batalha está quase ganha pelo Discernador.
Assim, a mentira de que a Água não é potável permanece, e o Poder do Discernador diz que a água que o Discernador dá, é potável e pode-se beber, embora tenha químicos venenosos. O Discernador diz que esses químicos tornam a água potável. Estes químicos são o porquê de haver água contaminada, e contaminar o algo faz parte do plano do Discernador.
Muito pouca Água resta sem químicos do Discernador, logo esta batalha está quase "ganha" pelo Discernador.
E aqueles que o Discernador permitirá e permite, podem receber um bocadinho de água do Discernador. Podem comprar água.
Estando a batalha pela Água, quase "ganha" pelo Discernador, o Discernador já tem planos para, mais, uma nova batalha, a batalha pelo Ar.
Assim está a nascer o Ar puro do Ar **não** puro. E mais uma vez, para justificar a existência de Ar **não** puro, o Discernador e os seus apoiantes na batalha do Ar, têm estado a enviar químicos para o Ar, têm estado a poluir o Ar, têm estado a criar Ar **contaminado**.
Primeiro, a criação do Ar poluído fez-se e continua-se a fazer, através da introdução de químicos na Água, pois o Ar, a Água, a Terra estão dentro de um ciclo, e o ciclo da circulação da Água.
A introdução de químicos na Água tornar a água potável, é aceite pelos Humanos, por muitos Humanos. Com esta mentira a batalha pelo Ar irá ser realizada.
E como a poluição de Ar pela Água é um processo lento, resolveu-se poluir directamente o Ar.
Continuando assim, nascerá o Ar **contaminado** (o Ar **não potável**).
O Discernador para justificar tal mentira, irá envenenar e está a envenenar o Ar com ajuda dos apoiantes do Discernador, pois os apoiantes do Discernador têm o Poder do Discernador.
E sendo assim, nascerá o discernimento entre ar puro e ar **não** puro.
E para a batalha do Ar ser "ganha" pelo Discernador, para atingir isso, a continuação do envenenamento do Ar tem continuado.
Nasceu uma nova doença, e muitas outras também, que se propaga pela ar.
E COVID é uma delas. O COVID é uma doença que talvez venha do envenenamento do ar, para justificar a existência de ar **não** puro, para o Discernador ter Poder sobre o Ar.
E assim, o Discernador consegue dar um pouco de Ar a quem pagar, e o Discernador passa a dizer quem pode ter Terra, quem pode ter Água, quem pode ter Ar.
E os apoiantes do Discernador, Poderão receber Ar, até que o Discernador decida que a nova batalha talvez seja a Vida, e após essa batalha só com a autorização do Discernador Poderar-se-á comprar Vida. O Discernador poderá ter ambições de ser o dono da Vida, e para tal já se começou a Discernir os algos que podem ter semente e dar Vida. Por exemplo, já se começou a plantar árvores de fruto que dão frutos sem caroço ou com menos caroço que a sua naturalidade, e ainda por cima contra a vontade das árvores de fruto.
O mesmo plano, do Discernador, foi aplicado no batalhar a Terra, no batalhar a Água, no batalhar o Ar, com os mesmo fins. Basta olhar e pensar porque tem de se pagar para ter Terra e lembrar porquê que a Água está **não potável** e perceber porquê que o Ar está a ser contaminado (pelo menos até hoje), para entender o conhecimento deste post.
Os Humanos continuaram a ter Terra, Água, Ar se deixarem de dar o Poder ao Discernador.
Os apoiantes do Discernador também são Humanos e podem deixar de dar poder ao Discernador, para os Humanos continuarem a ter Terra, Água, Ar.
Apenas deixar o plano de guerra de o Discernador.  Pouco a pouco, a tentar entender.
O Sol brilha e brilha para todos.
