---
layout: post
title: Já ganhamos
date: 2019-01-29
category: pensamentos
keywords: sonho, ganhar, dinheiro, meu
photo:
  filename: caça-sonhos.jpg
  alt: um caça sonhos
  creator:
    name : Pixabay
    site: https://www.pexels.com/@pixabay
---
Hoje tive um pequeno sonho ensinou-me uma grande lição. Vou partilhar neste post para que a sua lição não seja perdida pela minha memória que não é boa a agarrar-se a coisas :yum:.

<!-- stop -->

Estava na plateia de num campo de hóquei a ver um jogo, que iria começar mas que ainda não tinha começado. Tinha deixado uma mala num banco da plateia.
Ao voltar de fazer qualquer coisa, fui até à minha mala. Quando abro a bolsa da frente da minha mala vejo que tenho muito dinheiro (muito para mim) em euros dentro dela. Ver esse dinheiro na minha mala foi uma surpresa pois foi como se alguém tivesse posto esse dinheiro lá... Foi um achado.
Fiquei contente mas um pouco inseguro porque tinha muito dinheiro e estava num sítio não muito seguro (nem muito inseguro, mas o dinheiro não estava no banco onde se guarda o dinheiro).
Decidi contar o dinheiro para saber quanto deste achado, que estava na minha mala, eu tinha. Ao contar o dinheiro o meu primo Rogério aparece e diz para irmos embora. Sugeriu irmos à casa do Jojó; íamos dormir lá ou qualquer coisa assim.
Eu disse que estava o Dani (um amigo, grande guarda-redes de hóquei) a jogar, como maneira de arranjar um desculpa para ficarmos um pouco mais de tempo no pavilhão a ver o jogo, enquanto realmente o que queria era contar o dinheiro todo que tinha.
Eu tiro o dinheiro todo para fora (parecia que estava a crescer cada vez mais) e vou pousando-o no banco da bancada onde tenho a minha mala, de maneira a contá-lo sem alguém se aperceber muito.
Até que aparece um ex-colega de turma do Rosário, que às vezes foi um pouco rival, e tira-me algum daquele dinheiro.
Eu não tolero aquele comportamento e vou para a luta, contra ele, de maneira a reaver o dinheiro roubado (era o meu dinheiro agora!). Enquanto luto deixo o resto do dinheiro todo em cima do banco (da bancada) desguardado, com qualquer um a possivelmente poder tirá-lo também, enquanto lutava. Pouco depois de ter entrado na luta é que caí na consciência disso, de que podia ficar sem dinheiro algum.
Não vi alguém a tirar-me dinheiro, mas no fim decidi voltar para ver que tinha perdido o dinheiro todo.

Muito estúpido e banal este sonho, diz o leitor, e porquê escrever sobre ele?

Ele tem algumas grande lições.

Primeiro, eu estava feliz naquele campo de hóquei a possivelmente ver um jogo em que um grande amigo ia jogar. Até que aparece uma coisa que parece ter mais valor, na qual eu me foco em guardar. Essa coisa nem minha era, mas eu decidi fazer dela minha para a poder guardar seguramente no banco (banco onde se guarda dinheiro). Para além do mais essa coisa (o dinheiro) estava **dentro da minha mala**, bastante seguro, mas isso não era suficiente para mim. Queria guardar no banco, por isso tinha de contar o dinheiro para saber, no futuro, se alguém tinha-me roubado algum.

Ao fazer dessa coisa, que me apareceu, *minha* foi o pensamento que me levou a ficar, no final, com um sentimento negativo de grande tristeza, aflição, sofrimento e pensamento de grande infortúnio me acontecera. Essa coisa não era minha, tinha-me aparecido.
Era uma coisa deste mundo (que melhor que o dinheiro para significar isso). E em vez de ficar grato por isso me ter aparecido, fiquei a querer ficar com isso só para mim, com o pensamento de que alguém tirar-me-á isso, e não pode porque é *meu*.....

E tirar aconteceu.

Em vez de continuar grato por ainda algum (ele só me tirou um bocado, eu ainda tinha muito) decidi partir para a luta, por aquilo ser meu e ele não ter o direito de ficar com isso. Para mim, ele tinha tido uma atitude que eu não conseguia tolerar.
E no fim acabei com nenhum dinheiro.

Todo este pensamento do *meu* levou-me fazer coisas que, visto de longe, são coisas loucas de se envolver, luta. E esta atitude levou-me a perder tudo o resto que ainda tinha.
Mas no fim, não perdi tudo, ganhei um grande sofrimento e pesar..........
<br><br>
Felizmente foi só um sonho, espero manter-se assim, e aprender com este bom sonho/lição.

Todas as coisas deste mundo, já existiam antes de entrarmos nele. Algumas foram apenas transformadas em outras coisas, mas já existiam.
1. Sê sábio(a) e não entres no jogo do **meu**, reconhecendo que tudo já existia antes de nascermos neste mundo, e que não tem pertence.
2. Logo só podemos transformar este mundo e no processo, talvez, transformamos-nos a nós próprios.
3. Aquilo em que *tu próprio* te transformas é importante, escolhe viver agradecido por tudo o que tens e tudo o que te aparece. (nota para mim próprio)

Concordas, discordas? Fala comigo, manda-me um email.

Até sempre! &nbsp;:cyclone: :star2:
<br><br><br>
**Nota:** A ironia do sonho é que o dinheiro apareceu-me na mala que estava no banco (banco de bancada) e o que eu queria fazer era por dinheiro no banco (banco de dinheiro). Engraçado como as palavras são as mesmas :laughing:
