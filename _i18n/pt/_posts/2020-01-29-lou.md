---
layout: post
title: Lou
date: 2020-01-28
category: fala
keywords: lou, ou não exclusivo
photo:
  filename: wall-with-another-door.jpg
  alt: a garage door and a door in a wall made of stone
  creator:
    name: Steve Johnson
    site: https://www.pexels.com/@steve
---
Se a sua tia pede:
> Quero qualquer coisa cinzenta ou qualquer flor

Encontrando uma flor cinzenta, seria uma boa prenda. Neste caso seria um 'e'.  
Se encontra-se uma orquídea (flor), a prenda seria um 'ou'. Um casaco cinzento (coisa cinzenta) seria também um 'ou'.
<!-- stop -->

E se encontrar uma tulipa (flor) e um livro cinzento (coisa cinzenta) e desse os dois como prenda, estaria a não realizar o desejo da sua tia?

-- Depende...

-- Do quê??

-- Do tipo de 'ou'. Se o 'ou' é exclusivo ou não.

Apenas perguntando à sua tia é que saberá... Neste caso, a mim, parece-me ser um *ou não exclusivo*.

## Ou exclusivo / Ou não exclusivo

***Ou exclusivo***: é quando das 2 (ou mais) opções, quer-se só apenas uma  
***Ou não exclusivo***: é quando das 2 (ou mais) opções, não se importe de obter uma ou 2/mais das opções

Vou chamar o *ou exclusivo* de **ou** (pronunciando *oú*) ; e o *ou não exclusivo* de **lou**.


(*à parte*: uma flor cinzenta seria válido para realizar o desejo da sua tia, pois entra numa das opções)
