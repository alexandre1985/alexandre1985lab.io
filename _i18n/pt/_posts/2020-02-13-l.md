---
layout: post
title: "O ínicio da fala quântica: l"
date: 2020-02-13
category: fala
keywords: fala quântica, conceito l, união fala quântica com fala concreta
photo:
  filename: abstract-lights.jpg
  alt: tunnel of spining lights made by camera timelapse
  creator:
    name: Pixabay
    site: https://www.pexels.com/@pixabay
---

Até então, a fala tem o *e*, *[li][li]*, *ou*, *[lou][lou]*. Reparando melhor consigo ver que o *lou* tem dois componentes
<!-- stop -->
:

1. quando representa todos os membros do que se está a interligar
2. quando representa apenas um desses membros

Portanto, tenho de discernir o *lou*.

O 1º componente do *lou* (*1.*) já tenho um conceito, a que chamei de *e*.

O 2º componente (*2.*) também já tenho um conceito, a que chamei de *ou*.

Logo, o *lou* representa a possibilidade de um desses dois componentes ser verdadeiro.

Assim, dentro do *lou*, encontramos 3 conceitos: o *e*, o *ou* e a *possibilidade*.

Este último conceito (a *possibilidade*) chamarei de *l* (lendo-se *le* em Português).

Com o *l* entramos no reino das possibilidades, também chamado de reino do quântum e do reino popularmente conhecido como esotérico (que eu pouco conheço, pouco ouvi falar).

Não pretendo entrar muito neste reino do quântum, deixando apenas o ponto de união, em que a fala concreta (fala que estudei nestes 3 últimos posts) e a fala quântica convergem-se.

Esse ponto é o *l* (lendo-se *le* com uma paragem no *e*).

Nestes 3 últimos posts; que foram [lou][lou], [li][li] li agora com o [l][l]; o estudo da fala é/tornou-se *[imi][imi]* (não verdadeira, não falsa).


*Nota engraçada:* É engraçado que, a fala de *quântum* e *tanto* é parecida. Há a possibilidade de significarem a mesma coisa :smiley:

*Nota:* O *l* é um L minúsculo, não um i maiúsculo.

[lou]:{{site.posts[-20].url}}
[li]:{{site.posts[-21].url}}
[l]:{{page.url}}
[imi]:{{site.posts[-14].url}}
