---
layout: post
title: Os fundamentos da Cifragem
date: 2024-10-31
category: tecnologia
keywords: gnupg, pgp, librepgp, openpgp, cifragem, cifrar, cifração, assinar, assinatura, autenticar, autenticacao, certificar, assinar chaves
photo:
  filename: keys-on-the-door.jpg
  alt: a door with a pair of keys on the keyhole
  creator:
    name : AS Photography
    site: https://www.pexels.com/@asphotograpy/
---

As chaves de cifragem (de criptografia) tem certas capacidades. Cada capacidade corresponde ao que se consegue fazer com a chave.
<!-- stop -->

Com uma chave de cifragem podes fazer as seguintes coisas:

 - Assinar
 - Cifrar (encriptar)
 - Autenticar
 - Certificar

Esta capacidades estão atreladas a uma chave (ou ao par de chaves pública e privada). E esta chave é a tua identidade. É semelhante ao teu cartão de cidadão.

## Assinar

Assinar (com cifragem) é uma maneira digital de por uma assinatura em algo.

O valor, que esta assinatura tem, é dependente do que se assina. Basicamente, corresponde ao mesmo que fazer a tua assinatura à mão.

Se for um extrato do banco, podes estar a dar a permissão para que o teu dinheiro saia da tua conta (se for isso que o documento assinado diz); se for um livro que escreveste, é o mesmo que fazeres a tua assinatura de autor. Se assinares um email, serve como verificação que foste mesmo tu (o dono da chave) que enviaste esse email, e não uma qualquer outra pessoa na Internet que se pode passar por ti (qualquer pessoa pode forjar o campo "From" de um email).

Uma assinatura tem dupla função. Serve, quer para verificar que o conteúdo não ficou corrompido durante o trânsito da comunicação, quer para assegurar ao destinatário que esse conteúdo, realmente, veio do dono da chave (tu).

## Cifrar

Cifrar algo, serve para esconder o seu conteúdo, e guardar o resultado num lugar que está à vista. Daí eu preferir a palavra "cifrar" que a palavra "encriptar", pois, algo que foi cifrado, guarda-se num sítio acessível; não dentro de uma cripta inacessível.

Apenas a pessoa que contém o "segredo" (a pessoa que é dona da chave de destino da cifração) é que consegue decifrar esse conteúdo que foi cifrado. Mas qualquer um pode ver o ficheiro na sua forma cifrada (ilegível).

## Autenticar

Autenticar tem a mesma funcionalidade que uma password de login. Serve para dizer que és tu que estás a tentar ter acesso a um sítio remoto qualquer da Internet.

## Certificar

Certificar aplica-se a uma outra chave, e é a maneira de dizer que confirmaste que a informação, que está nessa outra chave, está correta.

Uma chave tipo PGP, leva a informação de nome da pessoa, endereço(s) de email, a impressão digital da chave (que é o número identificativo, e único, da chave), e uma foto opcional. Ao certificar, estás basicamente a confirmar, como verdadeiras, estas informações de uma dada chave.

Também existem as chaves tipo X.509.

O GnuPG é um Software Livre que usa, quer as chaves tipo PGP, quer as chaves tipo X.509. Mais informações em: [GnuPG.org](https://www.gnupg.org/).
