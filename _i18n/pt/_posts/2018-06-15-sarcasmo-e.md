---
layout: post
title: Sarcasmo é
date: 2018-06-15
category: pensamentos
keywords: sarcasmo, verdade, felicidade
photo:
  filename: kid-smiling.jpg
  alt: criança a rir
  creator:
    name : Alexander Dummer
    site: https://www.pexels.com/@alexander-dummer-37646
---
Sarcasmo é a tua intenção não estar alinhado com as tuas palavras. É uma maneira de não realizares o teu dever neste mundo.  
Acaba por ser fugires de ti próprio.  
Abandona esse hábito. Serás mais feliz. :rainbow:  
<!-- stop -->
##### Caso te esqueças
Enfrenta o medo de dizer o que sentes.  
Sarcasmo não traz benefícios.  
*(and be happy :smile:)*
