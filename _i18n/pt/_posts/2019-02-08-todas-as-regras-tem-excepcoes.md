---
layout: post
title: Para todos os seguidores de regras
date: 2019-02-08
category: pensamentos
keywords: regras, lição, conhecimento
photo:
  filename: road-in-nature.jpg
  alt: natureza com um caminho
  creator:
    name : Pixabay
    site: https://www.pexels.com/@pixabay
---
### Realização
Quando uma regra não funciona, substituí-la por outra regra, é permanecer num ciclo sem fim.

<!-- stop -->

### Conselho
Segue a tua intuição. (considero que a intuição não é dualista)

A mente lógica/racional, se for vista como uma mente de contrários, é uma mente de verdadeiros ou falsos, certos ou errados. Logo é dualista.
