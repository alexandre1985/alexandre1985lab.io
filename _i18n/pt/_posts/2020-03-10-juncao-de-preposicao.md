---
layout: post
title: Junção de Preposições (ilustrado)
date: 2020-03-10
category: fala
keywords: nova abordagem de operadores da fala, português, novos operadores da fala ilustrado
photo:
  filename: baby-and-boy.jpg
  alt: um bebé pequeno mais o seu irmão, que também é pequeno, deitados numa cama
  creator:
    name: Pixabay
    site: https://www.pexels.com/@pixabay
zoom: true
---

A junção de preposições requer uma nova abordagem segundo [esta publicação][problema-e]. *Junção de preposição* pode também ser chamado *operador da fala*.

A junção de preposições são os elementos que caracterizam a ligação.
<!-- stop -->

No caso da fala, ligação de preposições.

Como imagens, explicam melhor que palavras; deixo em baixo imagens que demonstram cada *junção de preposição*.

A junção de proposição está representada(s) a cor preta.


- Lou ([publicação][lou])

{% include component/blog-img.html lazy=true src='/assets/img/blog/other/lou.png' width-original=1300 alt='diagrama de lou, utilizando diagrama de venn' license-name='CC0' license-link='https://creativecommons.org/publicdomain/zero/1.0/' %}

- Ou (sem publicação)

{% include component/blog-img.html lazy=true src='/assets/img/blog/other/ou.png' width-original=1000 alt='diagrama de ou, utilizando diagrama de venn' license-name='CC0' license-link='https://creativecommons.org/publicdomain/zero/1.0/' %}

- Li ([publicação][li])

{% include component/blog-img.html lazy=true src='/assets/img/blog/other/li.png' width-original=500 alt='diagrama de li, utilizando diagrama de venn' license-name='CC0' license-link='https://creativecommons.org/publicdomain/zero/1.0/' %}

- Ni ([publicação][ni])

{% include component/blog-img.html lazy=true src='/assets/img/blog/other/ni.png' width-original=500 alt='diagrama de ni, utilizando diagrama de venn' license-name='CC0' license-link='https://creativecommons.org/publicdomain/zero/1.0/' %}

- E (sem publicação)

{% include component/blog-img.html lazy=true src='/assets/img/blog/other/e.png' width-original=500 alt='diagrama de e, utilizando diagrama de venn' license-name='CC0' license-link='https://creativecommons.org/publicdomain/zero/1.0/' %}

#### Nota

Também temos o [l][l] (lesse *le* com meia paragem no *e*) que define as possibilidades.

O *lou* tem três *l* (3*l*); o *ou* tem dois *l* (2*l*); os restantes têm um *l* (1*l*).


[problema-e]:{{site.posts[-24].url}}
[lou]:{{site.posts[-20].url}}
[li]:{{site.posts[-21].url}}
[l]:{{site.posts[-22].url}}
[ni]:{{site.posts[-23].url}}
