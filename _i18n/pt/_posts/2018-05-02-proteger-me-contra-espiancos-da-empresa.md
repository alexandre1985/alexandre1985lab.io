---
layout: post
title: Proteger-se contra espianços por parte da empresa em que se trabalha
date: 2018-05-02
category: tecnologia
keywords: pgp, mailvelope, email encriptado, espiar, openpgp
photo:
  filename: camera-closeup.jpg
  alt: câmera de filmar muito perto
  creator:
    name : Terje Sollie
    site: https://www.pexels.com/@solliefoto
---
A última empresa em que trabalhei, espiava os seus funcionários fortemente. Chegavam ao ponto de conseguirem ver e controlar o monitor de cada funcionário dentro da empresa. E ao fazerem isto, chegou-se a rir de divertimento do que estavam a ver e a fazer. Eu estive lá, e sei (fazia parte do departamento de informática).
<!-- stop -->
E isto não me parece que é um caso extraordinário, parece-me ser norma comum.
Infelizmente os valores morais não prevalecem nas empresas como a última em que trabalhei. :rage:

Mas vim dizer-vos que podemos proteger-nos contra certos casos de espionagem por parte das empresas. O caso especifico que vou falar, é o caso de encriptarmos os nossos emails, para só nós e o destinatário os podermos ler. Porreiro ah? Vamos lá a isso

## PGP
PGP que significa Pretty Good Privacy (Privacidade Bastante Boa) é um programa de encriptação end-to-end. End-to-End significa que a encriptação é feita no vosso computador, dando-vos garantias de que *mais ninguém* conseguir ler essa mensagem/ficheiro encriptado. E a desencriptação é feita no computador do destinatário.
Uma chave PGP contém duas componentes, uma chave pública e uma chave privada. A vossa chave pública é para partilhar com a pessoa com que quer-vos enviar uma mensagem encriptada; e a chave privada nunca podem partilhar, é só vossa e serve para desencriptar as mensagens encriptadas que são enviadas para vocês. Podem configurar uma password na vossa chave privada, para terem a certeza que só vocês conseguem usar a vossa chave privada.

## Mailvelope para salvar o dia
Agora vou falar-vos de uma ferramenta de encriptação de emails para quem utiliza o navegador de internet para ler os emails da empresa ou emails pessoais (a maioria de nós).
Chama-se [Mailvelope](https://www.mailvelope.com) e funciona no Chrome ([download aqui](https://chrome.google.com/webstore/detail/kajibbejlbohfaggdiogboambcijhkke)) e no Firefox ([download aqui](https://addons.mozilla.org/firefox/downloads/latest/mailvelope/)).
É uma extensão para o navegador de Internet e só têm de instalar a extensão para poderem começar a encriptar os emails.
Depois, têm de gerar uma chave, fornecendo uma nova password (não a password do teu email ou da conta da tua empresa), e fornecendo também o vosso nome e email, para outras pessoas saberem a quem pertence a chave pública. O par de chaves (chave pública mais chave privada) é uma chave PGP.

Após isso, já tem *quase* tudo para começar a enviar emails que só vocês e o destinatário podem ler.
Qualquer outra pessoa que tente ler os vossos emails a partir do servidor (o teu chefe ou o departamento de informática, por exemplo) só vai ver um conjunto de letras e números indecifrável.

Ahh, uma coisa importante. Para poderem enviar email encriptado para um endereço, têm de ter a chave pública do destinatário do vosso email. Por isso o destinatário também tem de ter o Mailvelope (ou outro software que gere chaves PGP). Depois, ambos têm de adicionar a chave pública do destinatário do email, ao vosso Mailvelope, e ambos já podem trocar emails encriptados, e de maneira segura.
Um bocadinho díficil de perceber lido assim por escrito, mas usando o software Mailvelope tudo vai fazer sentido.

## Conclusão
*Para resumir*, ambos têm de ter o Mailvelope e cada um tem de ter o seu próprio par de chave PGP, criada pelo seu próprio Mailvelope.
Para enviarem emails encriptados para uma pessoa, tem de ter a chave pública dessa pessoa. Da mesma maneira, se alguém quer enviar emails encriptados para vocês, essa pessoa tem de ter a vossa chave pública.
Esta troca de chave pública pode ser feita por email, pessoalmente ou utilizando um servidor de armazenamento de chaves públicas (chamado em inglês de *keyserver*).
Utilizando um keyserver, podem pesquisar por uma chave pública, usando o email do dono dessa chave. Essa chave pública tem de ter sido anteriormente submetida (feita o upload) para o keyserver em questão (um keyserver popular é o [keyserver.ubuntu.com](https://keyserver.ubuntu.com)). Isto faz-se visitando a página do keyserver e copiando e colando a vossa chave pública nessa página, clicando em "submeter" posteriormente.

Para saberem mais sobre o tema desta página, pesquisem: PGP, OpenPGP ou Mailvelope. Isto não é coisa de criminosos, é o direito fundamental à privacidade. Existem vários vídeos no Youtube a explicar este tema.

Dúvidas ou questões, é só mandar-me um email :wink:

Com grande respeito

{% include element/first-name.txt %}
