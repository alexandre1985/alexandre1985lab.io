---
layout: post
title: Quase pronto
date: 2018-06-07
category: pensamentos
keywords: obstáculo, aprendizagem, momento difícil
photo:
  filename: pensamentos.jpg
  alt: caderno em branco com uma caneta
  creator:
    name: me
---
Quando estamos a trabalhar profundamente numa situação, a caminho cada vez mais perto da perfeição, (às vezes) aparece um percalço numa outra área que nos afecta profundamente.  
Temos que parar o nosso trabalho para lidar com este percalço.  
<!-- stop  -->
A verdade é que o percalço ensina que havia algo que não estavas a valorizar e só te apercebeste do seu valor quando o percalço se instalou.  
Aprende e valoriza as coisas mais básicas e simples.  
Começa agora
