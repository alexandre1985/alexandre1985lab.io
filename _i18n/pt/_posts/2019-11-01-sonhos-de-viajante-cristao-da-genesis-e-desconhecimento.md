---
layout: post
title: Sonho para todos os Cristãos
date: 01-11-2019
category: divino
keywords: sonho, adão e eva, conhecimento, não conhecimento
photo:
  filename: raio-crepuscular.jpg
  alt: vista do céu nublado com raio crepuscular
  creator:
    name : Min An
    site: https://www.pexels.com/@minan1398
---

Hoje sonhei. O que descobri e partilharei neste blog, é de valor altíssimo para qualquer pessoa. Se essa pessoa tiver tido uma instrução Cristã acredito que o valor será indescritível.

Neste sonho passei por diversas realidades, de maneira gentil e descansada.

<!-- stop -->

O conhecimento é:

Na bíblia sagrada, podemos localizar o início, que se refere ao livro do *Génesis*. Conhecendo a história caminharás a um estado paradisíaco.

A localização (no *Genesis*) refere-se à história do Adão e Eva. Estes estão em um sítio perfeito (paraíso) e foram tentados a comer da *Árvore do Conhecimento*. Um fruto. A partir daí foram atirados para a Terra como castigo.

O comer do *fruto da Árvore do Conhecimento* significa ter *Conhecimento*. Todos nós, adultos temos *conhecimento*. As crianças tem em menor grau, do, chamado por senso comum, de *Conhecimento*.

Deixando o *Conhecimento* atingirás e vivenciarás o estado de paraíso.

Por deixando, eu digo, deixares de o (*conhecimento*) teres. Conhecimento é uma formatação do *sistema* que existe no planeta Terra (para a qual fomos atirados). Deixar o conhecimento é igual a buscar o *desconhecido*, e não pores *conhecimento* dentro deste estado. Pois, deixará de ser o *desconhecido*.

O engraçado e verdadeiro é que mesmo dentro deste sistema (do planeta Terra) que, hoje em dia, nos oferece o conhecimento abundantemente, consegues atingir este estado com *conhecimento*. *Conhecimento* esse = este post .

A pérola é desprenderes. O pensamento budista e indígena americano ajuda.



Falem comigo, usando [{{ site.email }}](mailto:{{ site.email }}). Estou sempre disposto a uma boa conversa.
