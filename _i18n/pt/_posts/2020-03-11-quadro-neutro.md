---
layout: post
title: Quadro Neutro (ilustrado)
date: 2020-03-11
category: numeros
keywords: conceitos matemáticos da realidade, algo
photo:
  filename: numbers-and-glasses.jpg
  alt: um bebé pequeno mais o seu irmão, que também é pequeno, deitados numa cama
  creator:
    name: Black ice
    site: https://www.pexels.com/@black-ice-551383
zoom: true
---

Este é um quadro que desenhei sobre conceitos matemáticos.
<!-- stop -->
{% include component/blog-img.html lazy=true src='/assets/img/blog/other/quadro-do-neutro.png' width-original=979 alt='ilustração sobre um diagrama meu que explica conceitos matemáticos' license-name='CC0' license-link='https://creativecommons.org/publicdomain/zero/1.0/' %}

#### Nota

Neste quadro o *Tudo* pode ser substituído por *Ponto*. Na primeira dimensão (dimensão do *Nada*, *Algo*, *Tudo*), o *Tudo* é o *Ponto*, por isso deixo em todas as dimensões como *Tudo*, para minha e vossa compreensão.

Segundo este quadro, também está demonstrado que estamos na 5ª dimensão, se as dimensões começarem com o número um.

Não na 3ª dimensão como nos é ensinado.

##### Errata

Na imagem, corrige o texto de *Figura Geométrica* para **Forma* Geométrica*.
