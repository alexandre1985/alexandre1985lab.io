---
layout: post
title: Meditação Que Ando a Fazer
date: 2018-02-27
categories: espiritual
keywords: meditação aro
photo:
  filename: lotus.jpg
  alt: planta de lótus
  creator:
    name : Pixabay
    site: https://www.pexels.com/@pixabay
---

## Introdução
Vou explicar a meditação que faço para você experimentar e ver se gosta. Os efeitos são imediatos mas antes de tirar conclusões, faça pelo menos durante uma semana, se puder todos os dias :wink:  
Esta meditação vem de um curso online grátis, em que se recebe emails semanalmente a instruí-lo, como realizar esta prática. Se quiser subscrever-se neste curso/newsletter online vá a [Aro Meditation site](http://aromeditation.org/) e subscreva o seu email para receber os emails directamente. É de graça. E estes emails vem em Inglês.
<!-- stop -->
Eu traduzi para Português um dos emails.


## Duração
O tempo de meditação é só de 5 minutos todos os dias. Se você se sentir com ambição, tente dez minutos. Veja como acha melhor.

## Horário
Pode fazer a qualquer altura do dia.

## Explicação
A meditação é enganosamente simples. Em certo sentido, as instruções completas são: "Esteja aqui - agora!"

Isto pode parecer não ter sentido. Você poderá responder: "Estou aqui, agora. Como é que poderia não ser assim?" O resto deste curso é dedicado a explicar como você pode não estar totalmente aqui, agora - e formas de voltar aqui e agora.

Há muito a dizer sobre meditação - o suficiente para preencher muitos livros. A meditação pode parecer complexa - mas isso é apenas porque os conceitos que usamos para entender nossas mentes são complexos. Durante este curso, você aprenderá como remover esses conceitos e olhar directamente para sua mente. Você aprenderá a experimentar a simplicidade, a clareza e o poder de a sua própria mente desconceituada.

## Práctica
Sente-se em algum sítio silencioso. Silêncio total não é necessário – mas música, barulho da televisão, ou pessoas a falar será distractivo. Alguns tipos de meditação podem ser realizados enquanto se ouve música – mas não este método. 

Sente-se confortavelmente. Sentar numa cadeira está ok. Se você estiver acostumadi a sentar-se sobre uma almofada no chão - e pode fazê-lo facilmente - isso é outra possibilidade. Sente-se razoavelmente erecti, mas não se esforçe para alcançar qualquer postura particular.

Use roupas soltas e confortáveis. Solte o seu cinto se estiver apertado.

Feche seus olhos quase totalmente, de maneira a que uma pequena luz entre, mas em que não fique em grande escuridão.

Quando pensamentos vêm – deixa-os vir. Quando pensamentos vão – deixa-os ir. Se você se encontra em envolvimento com uma corrente de pensamentos, deixe o seu envolvimento pensativo. Continue a deixar o envolvimento. Permaneça a deixar. Apenas deixe ir. Aconteça o que acontecer – deixe isso estar onde isso é.

Se você se sentir bem – não se agarre a esses pensamentos positivos. Se você se sentir mal – não rejeite esses pensamentos negativos. Especialmente importante: se você não sentir algo em particular – não se deixe ir na falta de sentimento.

Mantenha-se presente.

## Minha conclusão
Tenho praticado esta meditação à aproximadamente um mês e os resultados são agradáveis. Sinto que estou mais conectado com o meu coração e isso nota-se na minha fala, minha expressões, minha maneira de ser, e minha interacção com as pessoas e o mundo. Noto que as pessoas estão mais simpáticas comigo e isso é óptimo para mim. Isso é uma agradável surpresa.  
Partilho isto com você na esperança que você também possa receber estes aspectos agradáveis.
Tudo de bom :smiley:
Namasté
