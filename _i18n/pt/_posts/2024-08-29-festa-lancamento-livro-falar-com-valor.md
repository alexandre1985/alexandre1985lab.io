---
layout: post
title: Lançamento de 1º livro
date: 2024-08-29
categories: fala
keywords: falar com valor, livro falar com valor, primeiro livro de daniel cerqueira, 1º livro de daniel cerqueira, festa de lançamento de livro, festa de lançamento de 1º livro de daniel cerqueira, festa de lançamento de livro de daniel cerqueira, festa de lançamento do livro "Falar com Valor"
photo:
  filename: bunch-of-books.jpg
  alt: uma pequena pilha de livros, com um livro aberto no topo
  creator:
    name : Pixabay
    site: https://www.pexels.com/@pixabay
---

Vou fazer uma festa de lançamento do meu 1º livro. Este livro é sobre a fala e comunicação.

<!-- stop -->

## Com que então sempre escrevi um livro

Uma das minhas metas era a criação de uma página web pessoal. Então fiz este website. Outra das minhas metas era escrever um livro. Fiz-o, mas com o seu grau de nobreza, pois o assunto tinha de ser algo com o qual eu me sentisse apaixonado.

Graças à meditação e um pouco de consciência, consegui concluir uma coisa há uns tempos atrás: que a [Negação do Vazio][negacao-do-vazio] (é comum chamar-lhe de *Concordância Negativa*) fazia-me mal... punha-me em baixo. Após muitos anos inconsciente deste conhecimento, consegui realizar que isto era a causa de sofrimento que eu sentia, em determinadas situações, quando estava a comunicar com as pessoas. É algo, que maioritariamente, eu tenho nenhum controlo sobre, pois são as outras pessoas que costumam dizer [Negações do Vazio][negacao-do-vazio] (sempre tive uma certa consciência em evitar dizer *negações do vazio*, embora não soubesse bem o que essa expressão era -- apenas a sentia).

Após esta descoberta sobre a *Negação do Vazio*, também descobri outras falhas na comunicação que as pessoas, e a sociedade, normalmente cometem. Juntei tudo e escrevi um livro sobre estas falhas de comunicação da sociedade, dando soluções para estas falhas. Assim nasce o meu primeiro livro :smiley: .

## Festa é festa

Fui eu que criei, de raiz, este livro, mas precisei de ajuda na capa do livro. Para fazer a capa do meu livro, tive a ajuda de a minha companheira e de um amigo meu. Por isso, posso afirmar que, para chegar à versão final deste livro, foi preciso um trabalho de equipa.

Estava a pensar em apenas sair para celebrar a publicação do meu livro, mas dado ser o meu primeiro livro, decidi que vou fazer uma festa de celebração mais abrangente.

A festa de lançamento do meu livro, vai ser:

 - Data: dia 06 de Setembro 2024.
 - Local: cachacaria.bar (Rua de Miguel Bombarda nº 552, 4050-379 Porto).
 - Hora: 21h00.
 - Tipo: É aberta ao público (traz quem quiseres).

O objetivo é o convívio e ouvir uma pequena apresentação sobre o livro "Falar com Valor". Também celebrar o lançamento deste livro. Vai haver surpresas para quem participar no concurso que eu vou lançar durante a festa!

Aqui está o flyer desta festa de lançamento do livro...


{% include component/blog-img.html lazy=true src='/assets/img/blog/other/festa-de-lancamento-de-livro.jpg' width-original=1414 alt='flyer a anunciar a festa de lançamento do primeiro livro de Daniel Cerqueira' license-name='CC0' license-link='https://creativecommons.org/publicdomain/zero/1.0/' %}


## Uma amostra do meu primeiro livro

Deixo também aqui, nesta publicação de blog, um link para o download de uma amostra do meu livro. Para que possas confirmar se possuis interesse em comprar este livro.


<div class="text-center mb-4">
    <a class="btn btn-outline-info" href="/assets/books/falar-com-valor-amostra.pdf">
        <span class="file-pdf">
            &nbsp;Amostra do livro "Falar com Valor"
        </span>
    </a>
</div>


O livro "Falar com Valor" já se encontra disponível na [BlackWells.co.uk](https://blackwells.co.uk/), [Amazon.com](https://www.amazon.com/), [Amazon.com.br](https://www.amazon.com.br/), [Barnes & Noble](https://www.barnesandnoble.com/), [Kobo](https://www.kobo.com/), entre outras livrarias online. Para comprar, apenas tem de fazer a pesquisa, por este livro ("Falar com Valor"), dentro desses websites. O meu nome de autor é "Daniel Cerqueira S.".

[negacao-do-vazio]: {{site.posts[-4].url}}
