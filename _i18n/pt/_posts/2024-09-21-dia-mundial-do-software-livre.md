---
layout: post
title: Compilar o teu cerne Linux-Libre
date: 2024-09-21
categories: tecnologia
keywords: linux libre, linux-libre, compilar linux libre, compilar linux-libre, compilar linux libre embutido, compilar linux-libre embutido, compilar linux libre com módulos embutidos, compilar linux-libre com módulos embutidos
photo:
  filename: take-my-hand.jpg
  alt: com o mar como fundo, uma mão está aberta como a pedir que a agarrem, ou a simbolizar a liberdade de algo
  creator:
    name : Lukas
    site: https://www.pexels.com/@goumbik/
---

Hoje é Dia Mundial do Software Livre, e vou explicar como compilar o cerne Linux-Libre para a tua máquina.

<!-- stop -->

## Compilar Linux-Libre

Para compilar o Linux-Libre, tem de ter um sistema operativo GNU. Estes passos focam-se num sistema GNU, que não precisa de LVM, RAID, LUKS, e módulos semelhantes a estes, que são carregados quando se inicia um GNU. Isto porque esta configuração não precisará de um initramfs, para carregar o sistema.

### Compilar

Aqui vai os passos:

1. Instalar os pacotes para compilar o cerne. Se tens o `pacman`, executar: `pacman -Sy --needed --asdeps bc cpio gettext libelf pahole perl python tar xz`.
2. Fazer o download da versão desejada do Linux-Libre a partir do website da FSFLA [Linux-Libre Releases](http://linux-libre.fsfla.org/pub/linux-libre/releases/).
3. Extrair o ficheiro comprimido (por exemplo, `tar xf nome-do-ficheiro.tar.*`).
4. Fazer `cd` para a nova pasta.
5. Conectar todos os dispositivos que tens, ao computador, para que os módulos do cerne sejam carregados. Ou carregar manualmente todos os módulos que a tua máquina (e dispositivos) precisa.
6. Executar `yes "" | make localmodconfig` .
7. Podes desconectar os dispositivos (que não estás a precisar) do passo 5.
8. Executar `yes "" | make localyesconfig` .
9. Executar `make -j $(nproc --all)`.
10. Esperar menos de 20 minutos...

### Instalar

E quando a compilação tiver terminado com sucesso:

1. Executar como utilizador root `install -m 0600 "$(make -s image_name)" /boot/vmlinuz-$(make -s kernelrelease); install -m 0600 System.map /boot/System.map-$(make -s kernelrelease)` (ou, se preferires, `make install`). Tens de estar na mesma pasta da compilação anterior.
2. Configurar o bootloader para este novo cerne. Caso tenhas o GNU GRUB, só precisas de executar como utilizador root `grub-mkconfig -o /boot/grub/grub.cfg`.

Se tudo correu com sucesso, podes reiniciar o sistema e escolher o novo cerne durante o processo de inicialização do sistema.


## Dia Mundial do Software Livre

Desde 2004, a cada terceiro sábado do mês de setembro celebra-se o Dia Mundial do Software Livre, também chamado de *Software Freedom Day*.

Sendo hoje Dia Mundial do Software Livre, deixo esta publicação para saberes como ter um cerne Linux 100% Livre.

## Hardware Livre

Se estiveres a mudar do Linux para o Linux-Libre, vê se todos os componentes do teu PC tem suporte de hardware 100% livre. Para saberes, cola o resultado de `lspci -vmmnn` na caixa correspondente da página web [Search H-Node.org](https://h-node.org/search/form/en/). Isto dará um relatório de compatibilidade, em que podes ver a compatibilidade de todo o teu hardware com Linux-Libre (é comum as placas gráficas não terem suporte a aceleração 3D, tal como a minha não tem).

## Sobre Linux-Libre

GNU Linux-Libre é um projeto para manter e publicar distribuições 100% Livres de Linux, adequado para o uso em Distribuições Livres, removendo software que está incluído sem código-fonte, com código-fonte ofuscado ou obscurecido, ou sob licenças de software não-Livre. Pois estes não permitem que alteres o software para que faça o que desejas, e induz ou exige que instales peças adicionais de software não-Livre.

## Minha opinião

Passei a usar Linux-Libre (pois mudei para uma [Distribuição GNU 100% Livre](https://www.gnu.org/distros/free-distros.pt-br.html)) à uns anos atrás, e não tenho tido algum problema com o meu hardware. A mudança de Linux para Linux-Libre foi simples após pesquisar a fundo sobre Linux-Libre. Tive de trocar o cartão Wi-Fi do meu portátil, mas fora isso, tudo funcionou à primeira.

Com a compilação manual descrita nesta publicação, tenho a confiança que irei ter sempre um cerne atual (e mais rápido), não dependendo do sistema de pacotes (que pode ter os pacotes linux-libre desatualizados). Ter um cerne Linux-Libre atual é imperativo para uma computação mais segura e em liberdade.
