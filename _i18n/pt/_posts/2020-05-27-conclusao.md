---
layout: post
title: Uma conclusão deste blog em Português
date: 2020-05-27
keywords: lusitânia, luso, lusa, luzitânia, luzo, luza, final de blog português, fala portuguesa, luz, lus, lusi
photo:
  filename: candles-three.jpg
  alt: três velas de cores lindas, azuladas e arosadas, com a vela do meio sendo maior que duas dos lados
  creator:
    name: George Becker
    site: https://www.pexels.com/@eye4dtail
---
Após [este último post][ultimo-post] tenho a decisão de encerrar o meu blog em Português.
<!-- stop -->

A viagem que percorri com o blog em Português foi muito boa.
Guardo tudo o que me levou a escrever, cada um destes posts, e recordarei exactamente cada um.

Visitei o conceito, que como nenhum, está certo de quem é. Ultrapassei sofrimento através de meditação e cura, que é uma das minhas grandes lições. Durante este sofrimento, passei ligeiramente na segurança; que apesar de tudo, um sítio de crescimento e de maior certeza. Que fez-me encontrar um propósito. A linguagem tornou-se o escape ao sofrimento. É o propósito.

Daí vem o legado que deixo. O [pdf sobre a fala][fala-pdf] sendo a obra de legado de referência. E a [meditação Aro restruturada][aro-meditation-pt] a catapulta para o crescimento, na lingua Portuguesa.

Deixarei de focar-me no Português para abraçar projectos maiores; este ciclo para mim está realizado. Gostaria de ter escrito mais alguns conceitos sobre a fala, no entanto podem ler o lou, li, l, ni, e, etc. neste meu blog Português.

Deixo alguns dos apontamentos que faltam ser feitos. Alguns são:

- O 'nh' e o 'lh' serem apenas uma letra. Representam um som. O 'nh' tornar-se numa letra vogal, e em Português, o 'lh' pode ser uma letra consoante.
- Criar um termo absoluto para designar o meio. Como o termo absoluto de '*verdadeiro*' e o termo absoluto de '*falso*'. Eu chamei esse termo do meio de '*imi*'.
- Criar o género neutro, não masculino e não feminino, que acaba em 'i' (a [meditação que reestruturei][aro-meditation-pt] é uma obra que também tem este género).
- O você é estranho, o que o pode tornar-se espetacular. É um sujeito mas não é um sujeito. Em Português, os sujeitos são: eu, tu, ele, nós, vós, eles. Deixo a quem quiser, fazer algo com o '*você*', e deixando assim, espero que o potencial do '*você*' seja aproveitado.

Como nota, e é bem conhecido, que Portugal foi chamado de Lusitânia. Sendo cada pessoa masculina, chamado de luso, cada pessoa feminina chamada de lusa e (chamo eu) lusi à pessoa lusitana, seja a pessoa masculina ou seja a pessoa feminina. Através da dedução, podem ver que luso é uma pessoa masculina da luz (ou lus) e lusa é uma pessoa feminina da luz (ou lus). Lus(o)(a). Sendo Lusitânia o território da Luz (Lus).

Talvez daí, muitos Africanos desejem 'Muita Luz' em Português, provavelmente algo dito quando despediam-se dos Portugueses antigos (ou talvez fossem chamados de Lusitânos, não sei isso). Este dito nunca ouvi ser expressado por Portugueses e é algo muito carinhoso (de se dizer). Muita Luz é o desejado para os Portugueses ou antigos Lusitâni(os)(as).

Espero que tenha ficado claro. :sun_behind_large_cloud: :smiley:

[ultimo-post]:{{page.url}}
[fala-pdf]:/assets/other/fala-direita.pdf
[aro-meditation-pt]:/assets/other/Aro-Meditation-{{ site.lang | upcase }}.pdf
