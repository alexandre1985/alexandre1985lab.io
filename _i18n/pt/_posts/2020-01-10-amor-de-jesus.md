---
layout: post
title: Amor de Jesus
date: 2020-01-10
category: espiritual
keywords: história de jesus, cruz cristianismo, amor de jesus
photo:
  filename: couple-kissing.jpg
  alt: a man and a woman kissing
  creator:
    name : Vera Arsic
    site: https://www.pexels.com/@vera-arsic-304265
---

Tive uma teoria da conspiração que me diz que a história de que Jesus tenha feito amor (sexual) com uma/umas Maria(s) que lhe visitaram nos *últimos dias*, história abominada por muitas vertentes do Cristianismo, seja real.
<!-- stop -->

A maior e última mensagem que Jesus deixou foi:

> Amai-vos uns aos outros, tal como eu vos amei

significa que Jesus amou alguém (talvez sexualmente).

Isto é brilhante.

Esta teoria abre a possibilidade de Jesus ter tido um filho como também libertou, talvez a Maria Madalena, de se sentir *culpada* em fazer amor (por ser uma prostituta).

Tão lindo.

Jesus também disse que levava os pecados. Mais uma vez, não seria os pecados de pescar peixes (como os apóstolos faziam antes de Jesus chegar). Muito provavelmente seria o pecado de se entregar a homens sexualmente, sem amá-los. Dar amor sexual para sobreviver.

A última mensagem de Jesus, claramente enquadra-se a ouvintes femininos; sendo Jesus homem.

Amor perante mulher e libertação de pecados da prostituição de Maria Madalena.

Como nota; também disse muito claramente, para amarmos *uns aos outros*, humanos amarem humanos. Não amar uns aos outros **e Deus ou divindades também**. Amor de humano *para* humanos... (comecemos por amar humanos).

Um final muito melhor que andar com cruzes (do império romano) ao pescoço.... e em altares!!

Pois essas cruzes romanas tem como significado: Punição, Castigo e Morte.
Perpetuar isso, tal como a igreja cristã deseja? É isso que chamo de loucura (não o faças).
