---
layout: post
title: Os números e as realidades
date: 2019-02-11
keywords: realidade, números, vacuidade, unidade, dualidade, trindade, quatridade, pentaídade
photo:
  filename: athletics-lanes-with-numbers.jpg
  alt: faixas de pista de atletismo com números
  creator:
    name : Mateusz Dach
    site: https://www.pexels.com/@mateusz-dach-99805
---
Vou falar sobre os números que formam a nossa realidade do dia-a-dia (realidade material).

<!-- stop -->

Com o número 1 só podes obter uma coisa, sem variações e forma. Apenas um ponto pode existir. O movimento não existe, a forma não existe.

Com o 2 nasce um ponto fixo mais um extensão solta. Tipo uma cauda. Logo nasce a curva; e também nasce o movimento. Este movimento não existe só com um ponto; com um ponto só existe o tudo (ponto) ou o vazio.

Com o 3 nasce a linha (dois pontos mais uma extensão entre eles) e com a linha nasce a perceção (linha horizontal, linha vertical, linha diagonal).

O vazio existe desde o começo, o 0 (zero).
Portanto, podemos dizer que este mundo começa com o 0; com o 1 vem a noção de o tudo (1) ou o vazio (0) - existência. Com o 2 vem a curva e o movimento. E com o 3 vem a noção de perceção. Com o 4 vem a noção de figura (3 pontos mais um extensão entre eles, podendo desenhar um triângulo, uma figura geométrica). E com o 5 vem a noção de forma geométrica (4 pontos mais uma extensão entre eles, possibilitando a formação de uma pirâmide triangular).

Estas noções que falo são apenas possibilidades, visto que nem todos os 3 pontos unidos (unidos é, 3 pontos mais uma extensão) dão uma figura da 2ª dimensão (se estiverem 3 pontos com uma extensão na mesma linha) e 4 pontos unidos também podem não dar a formação geométrica (se tiverem 4 pontos com uma extensão no mesmo plano).
Mas são as condições mínimas para a existência dessas noções de realidades.

O nosso mundo do dia-a-dia penso estar assente no que os 4 pontos unidos (unidos, significa, mais uma extensão) podem formar - o espaço/forma/perceção/movimento/existência. É esta a dimensão que vivenciamos. Está confiantes que estão 4 pontos estão unidos, no mínimo; mais pontos também é possível e provável.

Espiritualmente falando, podemos fazer as seguintes associações:
- 0 -> vacuidade
- 1 -> unidade
- 2 -> dualidade
- 3 -> trindade
- 4 -> quatridade (acabei de inventar este nome :laughing:)
- 5 -> pentaídade (mais outra invenção :yum:)

Penso que isto pode servir de explicação porque é que os egípcios antigos construíram pirâmides. Eles podem possivelmente ter estado a falar sobre a nossa realidade e deixaram o símbolo base da nossa realidade - uma pirâmide.
